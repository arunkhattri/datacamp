from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import csr_matrix


# toy example
words = ['course', 'datacamp', 'potato', 'the']
vectorizer = TfidfVectorizer()
samples = vectorizer.fit_transform(words)
sparse_mat = csr_matrix(samples)

sparse_mat.toarray()


model = NMF(n_components=2, max_iter=400)
model.fit(sparse_mat)


nmf_features = model.transform(samples)


print(model.components_)


print(nmf_features)



samples[0, :].toarray()


nmf_features[0, :]


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


digits = pd.read_csv("../datasets/unsupervised_learning/lcd-digits.csv")
digits.shape


digit_0 = digits.iloc[0]
bitmap = np.array(digit_0).reshape((13, 8))
bitmap.shape


bitmap


plt.imshow(bitmap, cmap='gray', interpolation='nearest')
plt.colorbar()
plt.show()


def show_as_image(sample):
    bitmap = sample.reshape((13, 8))
    plt.figure()
    plt.imshow(bitmap, cmap='gray', interpolation='nearest')
    plt.colorbar()
    plt.show()


model = NMF(n_components=7)
features = model.fit_transform(digits)

for component in model.components_:
    show_as_image(component)


digit_features = features[0]
digit_features



