import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from utilities import permutation_samples


sns.set()
data_path = "../datasets/"
np.random.seed(5771)


# michelson speed of light data
michelson_sol = pd.read_csv(data_path + "michelson_speed_of_light.csv")
print(f"\n{michelson_sol.info()}")

# baseball no-hitter data
nohitter = np.loadtxt("./nohitters.txt")
print(f"nohitter.shape: {nohitter.shape}")

# us election data
swing_state = pd.read_csv(data_path + "2008_swing_states.csv")
print(f"\n{swing_state.info()}")


# Null hypothesis
# The click-through rate is not affected by the redesign.
#
# Permutation test of clicks through
# clickthrough_A, clickthrough_B: arr of 1s and 0s
clickthrough = np.random.choice([1, 0], 1000)
clickthroughA = clickthrough[:500]
clickthroughB = clickthrough[500:]


def diff_frac(dataA, dataB):
    fracA = np.sum(dataA) / len(dataA)
    fracB = np.sum(dataB) / len(dataB)
    return fracB - fracA


def permutation_samples(data1, data2):
    """genarate permutation samples"""
    samples = np.concatenate((data1, data2))
    perm = np.random.permutation(samples)

    perm_sample_data1 = perm[: len(data1)]
    perm_sample_data2 = perm[len(data1) :]

    return perm_sample_data1, perm_sample_data2


diff_frac_obs = diff_frac(clickthroughA, clickthroughB)
perm_replicates = np.empty(10000)

for i in range(10000):
    pA, pB = permutation_samples(clickthroughA, clickthroughB)
    perm_replicates[i] = diff_frac(pA, pB)

p_val = np.sum(perm_replicates >= diff_frac_obs) / 10000
print(f"p_val: {p_val}")
