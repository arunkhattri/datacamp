import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import pearsonr
from scipy import stats
from utilities import ecdf, draw_bs_reps
from utilities import draw_bs_pairs_linreg

sns.set()

data = "../datasets/"

df_swing = pd.read_csv(data + "2008_swing_states.csv")
print(f"\n{df_swing.info()}")

# pearson R
p_cc, p_val = pearsonr(df_swing["total_votes"], df_swing["dem_share"])

print(f"Pearson correlation coeffiecient: {p_cc}")

fig, ax = plt.subplots()
ax.scatter(df_swing.total_votes, df_swing.dem_share, s=2)
m, b = np.polyfit(df_swing["total_votes"], df_swing["dem_share"], 1)
x = np.array([np.min(df_swing.total_votes), np.max(df_swing.total_votes)])
y = m * x + b
ax.plot(x, y, c="green")
ax.annotate(f"ρ = {round(p_cc, 2)}", (442416.0, 36.14))
ax.set(
    title="2008 US swing state election results",
    xlabel="total votes (thousands)",
    ylabel="percent of votes for Obama",
)
plt.show()


# pearson correlation is 0.54

# Hypothesis test of correlation
# Null Hypothesis: there is no correlation between two varialbles
print(stats.ttest_ind(df_swing.total_votes, df_swing.dem_share, equal_var=False))


female_df = pd.read_csv("./f_illiteracy_fertility.csv")
print(f"\n{female_df.head()}")

illiteracy = female_df.illiteracy
fertility = female_df.fertility

# observed Pearson correlation between illiteracy and fertility
pr = np.corrcoef(illiteracy, fertility)[0, 1]
print(pr)

# scipy way
p_cc, p_val = pearsonr(illiteracy, fertility)

print(f"Pearson correlation coeffiecient: {p_cc}")

perm_replicates = np.empty(10000)

for i, _ in enumerate(perm_replicates):
    illiteracy_permuted = np.random.permutation(illiteracy)
    perm_replicates[i] = np.corrcoef(illiteracy_permuted, fertility)[0, 1]

p = np.sum(perm_replicates >= pr) / len(perm_replicates)
print(f"p_val: {p}")

# Darwin's Finces
finches = pd.read_csv("./finch.csv")
print(f"\n{finches.head()}")

sns.swarmplot(x="year", y="beak_depth", data=finches)
plt.xlabel("year")
plt.ylabel("beak depth (mm)")
plt.show()

# compute ecdf
bd_1975 = finches.loc[finches.year == 1975, "beak_depth"].values
bd_2012 = finches.loc[finches.year == 2012, "beak_depth"].values
x_1975, y_1975 = ecdf(bd_1975)
x_2012, y_2012 = ecdf(bd_2012)

fig, ax = plt.subplots()
ax.plot(x_1975, y_1975, marker=".", linestyle="None", label="1975")
ax.plot(x_2012, y_2012, marker=".", linestyle="None", label="2012")
ax.set(xlabel="beak_depth", ylabel="ECDF")
ax.legend()
plt.show()

# Compute the difference of the sample means: mean_diff
mean_diff = np.mean(bd_2012) - np.mean(bd_1975)

# Get bootstrap replicates of means
bs_replicates_1975 = draw_bs_reps(bd_1975, np.mean, 10000)
bs_replicates_2012 = draw_bs_reps(bd_2012, np.mean, 10000)

# Compute samples of difference of means: bs_diff_replicates
bs_diff_replicates = bs_replicates_2012 - bs_replicates_1975

# Compute 95% confidence interval: conf_int
conf_int = np.percentile(bs_diff_replicates, [2.5, 97.5])

# Print the results
print("difference of means =", mean_diff, "mm")
print("95% confidence interval =", conf_int, "mm")
# Compute mean of combined data set: combined_mean
combined_mean = np.mean(np.concatenate((bd_1975, bd_2012)))

# Shift the samples
bd_1975_shifted = bd_1975 - np.mean(bd_1975) + combined_mean
bd_2012_shifted = bd_2012 - np.mean(bd_2012) + combined_mean

# Get bootstrap replicates of shifted data sets
bs_replicates_1975 = draw_bs_reps(bd_1975_shifted, np.mean, 10000)
bs_replicates_2012 = draw_bs_reps(bd_2012_shifted, np.mean, 10000)

# Compute replicates of difference of means: bs_diff_replicates
bs_diff_replicates = bs_replicates_2012 - bs_replicates_1975

# Compute the p-value
p = np.sum(bs_diff_replicates >= mean_diff) / len(bs_diff_replicates)

# Print p-value
print("p =", p)

# We get a p-value of 0.0034, which suggests that there is a statistically
# significant difference. But remember: it is very important to know how
# different they are! In the previous exercise, you got a difference of 0.2 mm
# between the means. You should combine this with the statistical significance.
# Changing by 0.2 mm in 37 years is substantial by evolutionary standards. If it
# kept changing at that rate, the beak depth would double in only 400 years.

# beak length

bl_1975 = finches.loc[finches.year == 1975, "beak_length"].values
bl_2012 = finches.loc[finches.year == 2012, "beak_length"].values

# scatter plot of 1975 and 2012 data
fig, ax = plt.subplots()
ax.scatter(bl_1975, bd_1975, s=2, c="blue", label="1975")
ax.scatter(bl_2012, bd_2012, s=2, c="red", label="2012")
ax.set(xlabel="beak length (mm)", ylabel="beak depth (mm)")
ax.legend()
plt.show()


# Compute the linear regressions
slope_1975, intercept_1975 = np.polyfit(bl_1975, bd_1975, 1)
slope_2012, intercept_2012 = np.polyfit(bl_2012, bd_2012, 1)

# Perform pairs bootstrap for the linear regressions
bs_slope_reps_1975, bs_intercept_reps_1975 = draw_bs_pairs_linreg(
    bl_1975, bd_1975, 1000
)
bs_slope_reps_2012, bs_intercept_reps_2012 = draw_bs_pairs_linreg(
    bl_2012, bd_2012, 1000
)

# Compute confidence intervals of slopes
slope_conf_int_1975 = np.percentile(bs_slope_reps_1975, [2.5, 97.5])
slope_conf_int_2012 = np.percentile(bs_slope_reps_2012, [2.5, 97.5])
intercept_conf_int_1975 = np.percentile(bs_intercept_reps_1975, [2.5, 97.5])

intercept_conf_int_2012 = np.percentile(bs_intercept_reps_2012, [2.5, 97.5])


# Print the results
print("1975: slope =", slope_1975, "conf int =", slope_conf_int_1975)
print("1975: intercept =", intercept_1975, "conf int =", intercept_conf_int_1975)
print("2012: slope =", slope_2012, "conf int =", slope_conf_int_2012)
print("2012: intercept =", intercept_2012, "conf int =", intercept_conf_int_2012)

# linear regression results

fig, ax = plt.subplots()
ax.scatter(bl_1975, bd_1975, s=2, c="blue", label="1975")
ax.scatter(bl_2012, bd_2012, s=2, c="red", label="2012")

# plot the bootstrap line
x = np.array([10, 17])
for i in range(100):
    ax.plot(
        x,
        bs_slope_reps_1975[i] * x + bs_intercept_reps_1975[i],
        linewidth=0.5,
        alpha=0.2,
        color="blue",
    )
    ax.plot(
        x,
        bs_slope_reps_2012[i] * x + bs_intercept_reps_2012[i],
        linewidth=0.5,
        alpha=0.2,
        color="red",
    )

ax.set(xlabel="beak length (mm)", ylabel="beak depth (mm)")
ax.legend()
plt.show()

# Beak length to depth ratio
# ==========================
# The linear regressions showed interesting information about the beak geometry.
# The slope was the same in 1975 and 2012, suggesting that for every millimeter
# gained in beak length, the birds gained about half a millimeter in depth in both
# years. However, if we are interested in the shape of the beak, we want to
# compare the ratio of beak length to beak depth. Let's make that comparison.
# Compute length-to-depth ratios
ratio_1975 = bl_1975 / bd_1975
ratio_2012 = bl_2012 / bd_2012

# Compute means
mean_ratio_1975 = np.mean(ratio_1975)
mean_ratio_2012 = np.mean(ratio_2012)

# Generate bootstrap replicates of the means
bs_replicates_1975 = draw_bs_reps(ratio_1975, np.mean, 10000)
bs_replicates_2012 = draw_bs_reps(ratio_2012, np.mean, 10000)

# Compute the 99% confidence intervals
conf_int_1975 = np.percentile(bs_replicates_1975, [0.5, 99.5])
conf_int_2012 = np.percentile(bs_replicates_2012, [0.5, 99.5])

# Print the results
print(f"\n1975: mean ratio = {mean_ratio_1975}, conf int = {conf_int_1975}")
print(f"\n2012: mean ratio = {mean_ratio_2012}, conf int = {conf_int_2012}")

fig, ax = plt.subplots()
ax.plot(mean_ratio_1975, 1975, marker=".", linestyle="None")
ax.plot(mean_ratio_2012, 2012, marker=".", linestyle="None")
ax.plot(conf_int_1975, [1975, 1975])
ax.plot(conf_int_2012, [2012, 2012])
ax.set_yticks([1975, 2012])
ax.set(xlabel="beak length / beak depth", ylabel="year")
plt.show()

fortis = pd.read_csv("fortis.csv")
scandens = pd.read_csv("scandens.csv")

print(f"fortis: \n{fortis.info()}")
print(f"scandens: \n{scandens.info()}")

bd_parent_scandens = scandens.bd_parent_scandens.values
bd_offspring_scandens = scandens.bd_offspring_scandens.values
bd_parent_fortis = fortis.bd_parent_fortis.values
bd_offspring_fortis = fortis.bd_offspring_fortis.values

fig, ax = plt.subplots()
ax.scatter(
    bd_parent_scandens, bd_offspring_scandens, c="blue", alpha=0.5, label="scandens"
)
ax.scatter(bd_parent_fortis, bd_offspring_fortis, c="red", alpha=0.5, label="fortis")
ax.set(xlabel="parental beak depth (mm)", ylabel="offspring beak depth (mm)")
ax.legend()
plt.show()

# It appears as though there is a stronger correlation in fortis than in
# scandens. This suggests that beak depth is more strongly inherited in
# fortis. We'll quantify this correlation next.


def draw_bs_pairs(x, y, func, size=1):
    """Perform pairs bootstrap for a single statistic."""

    # Set up array of indices to sample from: inds
    inds = np.arange(len(x))

    # Initialize replicates: bs_replicates
    bs_replicates = np.empty(size)

    # Generate replicates
    for i in range(size):
        bs_inds = np.random.choice(inds, size=len(inds))
        bs_x, bs_y = x[bs_inds], y[bs_inds]
        bs_replicates[i] = func(bs_x, bs_y)

    return bs_replicates


def pearson_r(x, y):
    "Calculate pearson r"
    return pearsonr(x, y)[0]


# Compute the pearson correlation coefficients
r_scandens = pearson_r(bd_parent_scandens, bd_offspring_scandens)
r_fortis = pearson_r(bd_parent_fortis, bd_offspring_fortis)
print(f"\nr_scandens: {r_scandens}\nr_fortis: {r_fortis}")

# acquire 1000 bootstrap replicates of Pearson R
bs_replicates_scandens = draw_bs_pairs(
    bd_parent_scandens, bd_offspring_scandens, pearson_r, 1000
)
bs_replicates_fortis = draw_bs_pairs(
    bd_parent_fortis, bd_offspring_fortis, pearson_r, 1000
)

# compute 95% CI
ci_scandens = np.percentile(bs_replicates_scandens, [2.5, 97.5])
ci_fortis = np.percentile(bs_replicates_fortis, [2.5, 97.5])
print(
    f"\nr_scandens: {r_scandens} | ci_scandens: {ci_scandens}\nr_fortis: {r_fortis} | ci_fortis: {ci_fortis}"
)

# Measuring heritability
# ======================
# Remember that the Pearson correlation coefficient is the ratio of the covariance
# to the geometric mean of the variances of the two data sets. This is a measure
# of the correlation between parents and offspring, but might not be the best
# estimate of heritability. If we stop and think, it makes more sense to define
# heritability as the ratio of the covariance between parent and offspring to the
# variance of the parents alone. In this exercise, you will estimate the
# heritability and perform a pairs bootstrap calculation to get the 95% confidence
# interval.


def heritability(parents, offspring):
    """Compute the heritability from parent and offspring samples."""
    covariance_matrix = np.cov(parents, offspring)
    return covariance_matrix[0, 1] / covariance_matrix[0, 0]


# Compute the heritability
heritability_scandens = heritability(bd_parent_scandens, bd_offspring_scandens)
heritability_fortis = heritability(bd_parent_fortis, bd_offspring_fortis)

# Acquire 1000 bootstrap replicates of heritability
replicates_scandens = draw_bs_pairs(
    bd_parent_scandens, bd_offspring_scandens, heritability, size=1000
)
replicates_fortis = draw_bs_pairs(
    bd_parent_fortis, bd_offspring_fortis, heritability, size=1000
)

# Compute 95% confidence intervals
conf_int_scandens = np.percentile(replicates_scandens, [2.5, 97.5])
conf_int_fortis = np.percentile(replicates_fortis, [2.5, 97.5])

# Print results
print("G. scandens:", heritability_scandens, conf_int_scandens)
print("G. fortis:", heritability_fortis, conf_int_fortis)

# Here again, we see that G. fortis has stronger heritability than G. scandens.
# This suggests that the traits of G. fortis may be strongly incorporated into
# G. scandens by introgressive hybridization.

# Is beak depth heritable at all in scandens?
# ==============================================
# The heritability of beak depth in scandens seems low. It could be that
# this observed heritability was just achieved by chance and beak depth is
# actually not really heritable in the species. We will test that hypothesis
# here. To do this, we will do a pairs permutation test.
# Initialize array of replicates: perm_replicates
perm_replicates = np.empty(10000)

# Draw replicates
for i in range(10000):
    # Permute parent beak depths
    bd_parent_permuted = np.random.permutation(bd_parent_scandens)
    perm_replicates[i] = heritability(bd_parent_permuted, bd_offspring_scandens)


# Compute p-value: p
p = np.sum(perm_replicates >= heritability_scandens) / len(perm_replicates)

# Print the p-value
print("p-val =", p)

# we get a p-value of zero, which means that none of the 10,000 permutation
# pairs replicates we drew had a heritability high enough to match that which
# was observed. This strongly suggests that beak depth is heritable in scandens,
# just not as much as in G. fortis.
