import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr

sns.set()
data_path = "../datasets/"

# [-] Utitly Function


def ecdf(data):
    # number of data points
    n = len(data)
    x = np.sort(data)
    y = np.arange(1, n + 1) / n
    return x, y


np.random.seed(42)

# [-] get data
michelson_sol = pd.read_csv(data_path + "michelson_speed_of_light.csv")
print(f"\n{michelson_sol.info()}")

mu_msol = np.mean(michelson_sol["velocity of light in air (km/s)"])
sigma_msol = np.std(michelson_sol["velocity of light in air (km/s)"])
print(f"mu_msol: {mu_msol}\nsigma_msol: {sigma_msol}")

samples = np.random.normal(mu_msol, sigma_msol, size=10000)
x, y = ecdf(michelson_sol["velocity of light in air (km/s)"])
x_theor, y_theor = ecdf(samples)

fig, ax = plt.subplots()
ax.plot(x, y)
ax.plot(x_theor, y_theor, marker=".", linestyle="none")
ax.set(
    title="CDF of Michelson's measurement", xlabel="Speed of light (km/s)", ylabel="CDF"
)
plt.show()

# How do you know that mean and std dev from the data were the appropriate
# values for the Normal Parameters?
# Optimal parameters
# ==================
# Parameter values that bring the model in closest agreement with the data.

# nohitter (baseball), times between subsequent nohitter
nohitter = np.loadtxt("./nohitters.txt")
print(f"nohitter.shape: {nohitter.shape}")
ausat = np.mean(nohitter)
# exponential distrubution
antar_ausat_samay = np.random.exponential(ausat, 100000)
# plot the PDF
plt.hist(antar_ausat_samay, bins=50, density=True, histtype="step")
plt.xlabel("Games between no-hitters")
plt.ylabel("PDF")
plt.show()

# Do the above data follow our story
# Compute ecdf
x, y = ecdf(nohitter)
x_theor, y_theor = ecdf(antar_ausat_samay)

plt.plot(x, y)
plt.plot(x_theor, y_theor, marker=".", linestyle="none")
plt.margins(0.02)
plt.xlabel("Games between no-hitters")
plt.xlabel("Games between no-hitters")
plt.ylabel("ECDF")
plt.show()

# [-] Linear regression by least squares
swing_state = pd.read_csv(data_path + "2008_swing_states.csv")
print(f"\n{swing_state.info()}")

fig, ax = plt.subplots()
ax.scatter(swing_state["total_votes"], swing_state["dem_share"], s=2)
m, b = np.polyfit(swing_state["total_votes"], swing_state["dem_share"], 1)
x = np.array([np.min(swing_state.total_votes), np.max(swing_state.total_votes)])
y = m * x + b
ax.plot(x, y, c="green")
ax.set(
    title="2008 US swing state election results",
    xlabel="total votes (thousands)",
    ylabel="percent of vote for Obama",
)
plt.show()

p_cc, p_val = pearsonr(swing_state["total_votes"], swing_state["dem_share"])

print(f"Pearson correlation coeffiecient: {p_cc}")

# [-] Bootstrap confidence interval
# The use of resampled data to perform statistical inference
# resampling michelson's data

# example bootstrap replicate
print(np.random.choice([1, 2, 3, 4, 5], size=5))

# michelson's data
michelson_speed_of_light = michelson_sol["velocity of light in air (km/s)"]
bs_sample = np.random.choice(michelson_speed_of_light, size=100)
print(f"michelson_speed_of_light mean: {np.mean(michelson_speed_of_light)}")
print(f"bs_sample mean: {np.mean(bs_sample)}")
print(f"bs_sample median: {np.median(bs_sample)}")
print(f"bs_sample std: {np.std(bs_sample)}")

# bootstrapping 100 samples of speed of light
for _ in range(100):
    bs_sample = np.random.choice(michelson_speed_of_light, size=100)
    # compute and plot ECDF
    x, y = ecdf(bs_sample)
    plt.plot(x, y, marker=".", linestyle="none", color="grey", alpha=0.1)

# original data
x, y = ecdf(michelson_speed_of_light)
plt.plot(x, y, marker=".")
plt.margins(0.02)
plt.xlabel("speed of light (km/s)")
plt.ylabel("ECDF")
plt.show()

# [-] Bootstrap confidence intervals


def bootstrap_replicate_1d(data, func):
    """Generate bootstrap replicate of 1D data"""
    bs_sample = np.random.choice(data, len(data))
    return func(bs_sample)


bs_replicates = np.empty(10000)

for i in range(10000):
    bs_replicates[i] = bootstrap_replicate_1d(michelson_speed_of_light, np.mean)

# make a histogram
fig, ax = plt.subplots()
ax.hist(bs_replicates, density=True, bins=30)
ax.set(xlabel="mean speed of light (km/s)", ylabel="PDF")
plt.show()

conf_int = np.percentile(bs_replicates, [5, 95])
print(f"bootstrap confidence interval: {conf_int}")

# [-] Generating a pairs bootstrap sample

inds = np.arange(len(swing_state.total_votes))
bs_inds = np.random.choice(inds, len(inds))
bs_total_votes = swing_state.total_votes[bs_inds]
bs_dem_share = swing_state.dem_share[bs_inds]

bs_slope, bs_intercept = np.polyfit(bs_total_votes, bs_dem_share, 1)
print(f"\nbs_slope: {bs_slope}")
print(f"bs_intercept: {bs_intercept}")

# function for pairwise bootstrap replicate
def draw_bs_pairs_linreg(x, y, size=1):
    """Perform pairs bootstrap for linear regression."""

    # Set up array of indices to sample from: inds
    inds = np.arange(len(x))

    # Initialize replicates: bs_slope_reps, bs_intercept_reps
    bs_slope_reps = np.empty(size)
    bs_intercept_reps = np.empty(size)

    # Generate replicates
    for i in range(size):
        bs_inds = np.random.choice(inds, size=len(inds))
        bs_x, bs_y = x[bs_inds], y[bs_inds]
        bs_slope_reps[i], bs_intercept_reps[i] = np.polyfit(bs_x, bs_y, 1)

    return bs_slope_reps, bs_intercept_reps


# generate replicates of slope and intercept
bs_slope_reps, bs_intercept_reps = draw_bs_pairs_linreg(
    swing_state.total_votes, swing_state.dem_share, size=1000
)
print(f"95% CI for slope: {np.percentile(bs_slope_reps, [2.5, 97.5])}")

# plot
fig, ax = plt.subplots()
ax.hist(bs_slope_reps, bins=50, density=True)
ax.set(xlabel="slope", ylabel="PDF")
plt.show()

# plotting bootstrap regression
# A nice way to visualize the variability we might expect in a linear regression is to
# plot the line we would get from each bootstrap replicate of the slope and intercept.
# Doing this for first 100 bootstrap replicates

x = np.array([2245, 863486])

# plot the bootstrap line
fig, ax = plt.subplots()
for i in range(100):
    ax.plot(
        x,
        bs_slope_reps[i] * x + bs_intercept_reps[i],
        linewidth=0.5,
        alpha=0.5,
        color="red",
    )
ax.scatter(swing_state.total_votes, swing_state.dem_share, s=2)
ax.set(
    title="Bootstrap regressions", xlabel="total votes", ylabel="vote shares of Obama"
)
ax.margins(0.2)
plt.show()
