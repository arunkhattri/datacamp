import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

data_path = "../datasets/"

# files in data_path
for root, directories, files in os.walk(data_path):
    for directory in directories:
        print(directory, end=", ")
    for file in files:
        print(file, end=", ")

# load data
df_swing = pd.read_csv(data_path + "2008_swing_states.csv")
print(f"\n{df_swing.head()}")

# [-] Graphical EDA
# -------------

plt.style.use("seaborn")

fig, ax = plt.subplots()
ax.hist(df_swing["dem_share"], edgecolor="white")
ax.set(xlabel="Percent of vote for Obama", ylabel="Number of Counties")
plt.show()

# adjusting the bin edges

bin_edges = np.arange(0, 110, 10)

fig, ax = plt.subplots()
ax.hist(df_swing["dem_share"], bins=bin_edges, edgecolor="white")
ax.set(xlabel="Percent of vote for Obama", ylabel="Number of Counties")
plt.show()

# in most swing states Obama got less than 50% of votes

# [-] Bee swarm plot
## --------------

sns.swarmplot(x="state", y="dem_share", data=df_swing)
plt.xlabel("State")
plt.ylabel("Percent of vote for Obama")
plt.show()

# same observation can be confirmed from bee swarm plot with better clarity

# [-] ECDF: Empirical Cumulative Density Function
## -------------------------------------------

fig, ax = plt.subplots()

x = np.sort(df_swing.dem_share)
y = np.arange(1, len(x) + 1) / len(x)

ax.plot(x, y, marker=".", linestyle="none")
ax.set(xlabel="Percent of vote for Obama", ylabel="ECDF")
ax.margins(0.02)  # 2% percent margin aroun edges
plt.show()

# in 75% of swing state counties Obama got less than 50% vote

# function for ECDF


def ecdf(data):
    # number of data points
    n = len(data)
    x = np.sort(data)
    y = np.arange(1, n + 1) / n
    return x, y


# [-] Summary Statistics
# ------------------

## Mean vote percentage

dem_share_PA = df_swing.loc[df_swing["state"] == "PA", ["dem_share", "state"]]
print(np.mean(dem_share_PA.dem_share))


fig, ax = plt.subplots()
sns.swarmplot(x="state", y="dem_share", data=dem_share_PA, ax=ax)
ax.axhline(np.mean(dem_share_PA.dem_share))

plt.xlabel("State")
plt.ylabel("Percent of vote for Obama")
plt.show()

# median
PA = df_swing.loc[df_swing["state"] == "PA", "dem_share"]
print(np.median(PA))

# [-] computing percentiles
# =====================

percent_arr = np.array([5, 25, 50, 75, 95])
dem_share_percentile = np.percentile(df_swing["dem_share"], percent_arr)
print(dem_share_percentile)

sns.boxplot(x="state", y="dem_share", data=df_swing)
plt.show()

# [-] Comparing Percentiles to ECDF
# =============================

fig, ax = plt.subplots()

x = np.sort(df_swing.dem_share)
y = np.arange(1, len(x) + 1) / len(x)

ax.plot(x, y, marker=".", linestyle="none")
ax.plot(
    dem_share_percentile, percent_arr / 100, marker="D", color="red", linestyle="none"
)
ax.set(xlabel="Percent of vote for Obama", ylabel="ECDF")
ax.margins(0.02)  # 2% percent margin aroun edges
plt.show()

# [-] Variance and Standard Deviation
# ===============================

PA_dem_share = df_swing.loc[df_swing["state"] == "PA", ["dem_share"]]
OH_dem_share = df_swing.loc[df_swing["state"] == "OH", ["dem_share"]]
FL_dem_share = df_swing.loc[df_swing["state"] == "FL", ["dem_share"]]

# variance
print(f"Variance PA: {np.var(PA_dem_share)}")
print(f"Variance OH: {np.var(OH_dem_share)}")
print(f"Variance FL: {np.var(FL_dem_share)}")

# standard deviation
print(f"Std dev PA: {np.std(PA_dem_share)}")
print(f"Std dev OH: {np.std(OH_dem_share)}")
print(f"Std dev FL: {np.std(FL_dem_share)}")

sns.distplot(PA_dem_share)
plt.show()

# [-] covariance and the pearson correlation coefficient
# ==================================================

# total votes vs votes for obama

fig, ax = plt.subplots()
ax.scatter(df_swing["total_votes"], df_swing["dem_share"], s=2)
ax.axhline(y=np.mean(df_swing["dem_share"]), color="red", label="mean_dem_share")
ax.axvline(x=np.mean(df_swing["total_votes"]), color="green", label="mean_total_votes")
# ax.axhline(y=np.mean(df_swing['dem_share']),
#            xmin=min(df_swing['total_votes']),
#            xmax=max(df_swing['total_votes']),
#            label='mean_dem_share')
# ax.axvline(x=np.mean(df_swing['total_votes']),
#            ymin=min(df_swing['dem_share']),
#            ymax=max(df_swing['dem_share']),
#            label='mean_total_votes')
ax.set(
    title="Total votes vs percent of votes for Obama",
    xlabel="Total votes",
    ylabel="Percent of votes for Obama",
)
ax.legend()
plt.show()

# correlation matrix
cov = np.cov(df_swing.total_votes, df_swing.dem_share)
print(cov[0, 1])

# pearson correlation coefficient
pcc = np.corrcoef(df_swing.total_votes, df_swing.dem_share)[0, 1]
print(pcc)

# [-] Probablistic logic and statistical inference
# ================================================

# simulating 4 coin flips

np.random.seed(42)
rand_nums = np.random.random(size=4)
print(rand_nums)

heads = rand_nums < 0.5
print(f"heads: {heads}")
print(np.sum(heads))

# probability of getting 4 heads

n_all_heads = 0  # 4 heads
for _ in range(10000):
    heads = np.random.random(size=4) < 0.5
    n_heads = np.sum(heads)
    if n_heads == 4:
        n_all_heads += 1

print(f"Probability of 4-heads: {n_all_heads / 10000}")

# [-] The Binomial distribution
# =============================

# Probability Mass Function (PMF)
# The set of probabilities of discrete outcomes

# 10000 samples out of binomial distribution
# number of defaults,we would expect (p = 0.05) for 100 loans
n_defaults = np.random.binomial(n=100, p=0.05, size=10000)

bins = np.arange(0, max(n_defaults) + 1.5) - 0.5
plt.hist(n_defaults, bins=bins, density=True, edgecolor="white")
plt.xlabel("# of loan defaults per 100")
plt.ylabel("PMF")
plt.show()

# suppose there are on avreage 5 deliveries per day
# draw 10,000 samples out of Poisson distribution

n_deliv = np.random.poisson(5, 10000)
n_large = np.sum(n_deliv >= 10)
p_large = n_large / 10000
print(f"Probability of 6 or more delivery in a day: {p_large}")

# plot CDF
x, y = ecdf(n_deliv)
fig, ax = plt.subplots()
ax.plot(x, y, marker=".", linestyle="none")
ax.set(title="deliveries per day", xlabel="# of deleveries per day", ylabel="fraction")
plt.show()


# [-] Probability density functions
# ================================

m_delv = np.mean(n_deliv)
std_delv = np.std(n_deliv)
samples = np.random.normal(m_delv, std_delv, size=10000)
x_theor, y_theor = ecdf(samples)

fig, ax = plt.subplots()
ax.plot(x_theor, y_theor, label="sample")
ax.plot(x, y, marker=".", linestyle="none", label="n_delv")
ax.set(title="deliveries per day", xlabel="# of deleveries per day", ylabel="fraction")
ax.legend()
plt.show()
