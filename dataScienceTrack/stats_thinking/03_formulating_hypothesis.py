import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr

sns.set()
data_path = "../datasets/"
np.random.seed(5771)


# michelson speed of light data
michelson_sol = pd.read_csv(data_path + "michelson_speed_of_light.csv")
print(f"\n{michelson_sol.info()}")

# baseball no-hitter data
nohitter = np.loadtxt("./nohitters.txt")
print(f"nohitter.shape: {nohitter.shape}")

# us election data
swing_state = pd.read_csv(data_path + "2008_swing_states.csv")
print(f"\n{swing_state.info()}")


def bootstrap_replicate_1d(data, func):
    """Generate bootstrap replicate of 1D data."""
    bs_sample = np.random.choice(data, len(data))
    return func(bs_sample)


def draw_bs_reps(data, func, size=1):
    """Draw bootstrap replicates."""

    # Initialize array of replicates: bs_replicates

    bs_replicates = np.empty(size)

    # Generate replicates
    for i in range(size):
        bs_replicates[i] = bootstrap_replicate_1d(data, func)

    return bs_replicates


def ecdf(data):
    # number of data points
    n = len(data)
    x = np.sort(data)
    y = np.arange(1, n + 1) / n
    return x, y


# Hypothesis testing

# Ohio and Pennsylvania are similar states, they are neighbors, both having
# liberal urban counties and also lots of rural conservative counties.
# Hypotheses:
# county-level voting in these two states have identical probability distributions.

# Simulating the hypothesis
# 1. what the data would look like if the county level voting trends in the
# two states were identically distributed.
#
# [-] Generating a permutation sample
dem_share_PA = swing_state.loc[swing_state.state == "PA", "dem_share"]
dem_share_OH = swing_state.loc[swing_state.state == "OH", "dem_share"]

# function
def permutation_samples(data1, data2):
    """genarate permutation samples"""
    samples = np.concatenate((data1, data2))
    perm = np.random.permutation(samples)

    perm_sample_data1 = perm[: len(data1)]
    perm_sample_data2 = perm[len(data1) :]

    return perm_sample_data1, perm_sample_data2


for _ in range(50):
    perm_sample_PA, perm_sample_OH = permutation_samples(dem_share_PA, dem_share_OH)
    # compute ECDF's
    x1, y1 = ecdf(perm_sample_PA)
    x2, y2 = ecdf(perm_sample_OH)

    # plot
    plt.plot(x1, y1, marker=".", linestyle="none", alpha=0.02, c="r")
    plt.plot(x2, y2, marker=".", linestyle="none", alpha=0.02, c="b")

# plot ECDF from original data
x1, y1 = ecdf(dem_share_PA)
x2, y2 = ecdf(dem_share_OH)
plt.plot(x1, y1, marker=".", linestyle="none", c="r")
plt.plot(x2, y2, marker=".", linestyle="none", c="b")

plt.margins(0.02)
plt.xlabel("vote share for Obama")
plt.ylabel("ECDF")
plt.show()


# Notice that the permutation samples ECDF's overlap and give apurple haze.
# Permutation samples ECDF overlap the observed data,
# suggesting that hypothesis is commensurating with the data.
# Both are identically distrbuted.

# [-] Test statistics and p-values

perm_sample_PA, perm_sample_OH = permutation_samples(dem_share_PA, dem_share_OH)
mean_diff = np.mean(perm_sample_PA) - np.mean(perm_sample_OH)
print(mean_diff)

mean_actual_diff = np.mean(dem_share_PA) - np.mean(dem_share_OH)
print(mean_actual_diff)


def draw_perm_reps(data_1, data_2, func, size=1):
    """Generate multiple permutation replicates."""

    # Initialize array of replicates: perm_replicates
    perm_replicates = np.empty(size)

    for i in range(size):
        # Generate permutation sample
        perm_sample_1, perm_sample_2 = permutation_samples(data_1, data_2)

        # Compute the test statistic
        perm_replicates[i] = func(perm_sample_1) - func(perm_sample_2)

    return perm_replicates


perm_mean = draw_perm_reps(dem_share_PA, dem_share_OH, np.mean, 10000)

p = np.sum(perm_mean >= mean_actual_diff) / len(perm_mean)
print(f"\np-value: {p}")

# The p-value tells that there is about a 23.71% chance that we would get the
# difference of means observed in the election if all conditions remain same.
# A p-value below .01 is typically said to be 'statistically significant"

# Null Hypothesis:
# The true mean speed of light in Michelson's experiment was actually
# Newcomb's reported value
#
# we shift Michelson's data such that it's mean now matches Newcomb's value
michelson_speed_of_light = michelson_sol["velocity of light in air (km/s)"]
newcomb_value = 299860  # Km/s
michelson_shifted = (
    michelson_speed_of_light - np.mean(michelson_speed_of_light) + newcomb_value
)

print(michelson_shifted)

# Calculating the test statistics
def diff_from_newcomb(data, newcomb_value=299860):
    return np.mean(data) - newcomb_value


# mean of the bootstrap sample minus Newcomb's value
diff_obs = diff_from_newcomb(michelson_speed_of_light)
print(diff_obs)
