import pandas as pd
import numpy as np

bmi = pd.read_csv("bmi.csv")
print(bmi)

# Duplicate values
# get duplicates across all columns

dup_bmi = bmi.duplicated()
print(dup_bmi)

print(bmi[dup_bmi])

# properly calibrate duplicates

col_names = ["first_name", "last_name", "weight"]
dup_bmi_names = bmi.duplicated(subset=col_names, keep=False)
print(dup_bmi_names)

print()
print(bmi[dup_bmi_names])

print(bmi[dup_bmi_names].sort_values(by="first_name"))

# keep one of complete duplicate values
bmi.drop_duplicates(keep="first", inplace=True)
print(bmi)

col_names = ["first_name", "last_name", "weight"]
dup_bmi_names = bmi.duplicated(subset=col_names, keep=False)
print(dup_bmi_names)
print()
print(bmi[dup_bmi_names])

# remove other duplicates
summaries = {"height": "mean"}
bmi_grp = bmi.groupby(by=col_names).agg(summaries).reset_index()
print(bmi_grp)

bmi_grp["bmi"] = bmi_grp["weight"] / (bmi_grp["height"] ** 2)
print(f"\n{bmi_grp}")

# Membership constraints

study_data_dict = {
    "name": ["beth", "ignatius", "paul", "helen", "jennifer", "kennedy", "keith"],
    "birthday": [
        "2019-10-20",
        "2020-07-08",
        "2019-08-12",
        "2019-03-17",
        "2019-12-17",
        "2020-04-27",
        "2019-04-19",
    ],
    "blood_type": ["B-", "A-", "O+", "O-", "Z+", "A+", "AB+"],
}

study_data = pd.DataFrame.from_dict(study_data_dict)
print(study_data)

study_data["birthday"] = pd.to_datetime(study_data["birthday"])
study_data["blood_type"] = study_data["blood_type"].astype("category")
print(study_data.info())

categories = pd.DataFrame(
    {"blood_type": ["O-", "O+", "A-", "A+", "B+", "B-", "AB+", "AB-"]}
)
print(categories)


# finding inconsistent categories
inconsistent_cats = set(study_data["blood_type"]).difference(categories["blood_type"])
print(inconsistent_cats)

# get and print rows with inconsistent categories
inconsistent_rows = study_data["blood_type"].isin(inconsistent_cats)
print(study_data[inconsistent_rows])

# dropping inconsistent categories
consistent_data = study_data[~inconsistent_rows]
print(consistent_data)

demographics = pd.read_csv("household_survey.csv")
print(demographics.info())

print(demographics.head())

# converting to category column
for col in demographics.select_dtypes(include="object").columns:
    demographics[col] = demographics[col].astype("category")

print(demographics.info())

# creating categories out of data
print(demographics["household_income"].describe())

group_names = ["low", "medium", "high"]

demographics["inc_grp"] = pd.qcut(
    demographics["household_income"], q=3, labels=group_names
)
# (49080.999, 97751.667] < (97751.667, 167651.0] < (167651.0, 247614.0]

print(demographics[["inc_grp", "household_income"]])

# or can be done by defining own cuts
ranges = [0, 100000, 200000, np.inf]
group_names = ["low", "medium", "high"]

demographics["income_group"] = pd.cut(
    demographics["household_income"], bins=ranges, labels=group_names
)

print(demographics[["inc_grp", "household_income", "income_group"]])

# collapsing data into category
# map categories to fewer ones
mapping = {
    "car": "4wheeler",
    "sedan": "4wheeler",
    "suv": "4wheeler",
    "scooty": "2wheeler",
    "bike": "2wheeler",
}
demographics["veh_cat"] = demographics["vehicle_type"].replace(mapping)
print(demographics["veh_cat"].unique())
print(demographics["vehicle_type"].unique())
