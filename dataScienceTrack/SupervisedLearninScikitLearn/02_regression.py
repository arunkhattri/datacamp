import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import datasets
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso

sns.set()

data_file = "../datasets/"

# Boston Housing Data
# ====================
boston = datasets.load_boston()
print(f"\n{boston.keys()}")

boston_df = pd.DataFrame(boston.data, columns=boston.feature_names)
print(boston_df.info())

print(boston["DESCR"])

boston_df["MEDV"] = boston.target
print(f"\n{boston_df.head()}")

X = boston.data
y = boston.target

# predicting house value from a single feature
# "RM": average number of rooms per dwelling

X_rooms = X[:, 5]
print(f"type X_rooms: {type(X_rooms)}; shape: {X_rooms.shape}")
print(f"type y: {type(y)}; shape: {y.shape}")

# reshaping
X_rooms = X_rooms.reshape(-1, 1)
y = y.reshape(-1, 1)
print(f"X_rooms shape: {X_rooms.shape}")
print(f"y shape: {y.shape}")

# plot
fig, ax = plt.subplots()
ax.scatter(X_rooms, y, s=2)
ax.set(
    title="Boston Housing Data",
    xlabel="Value of House per 1000($)",
    ylabel="Number of Rooms",
)
plt.show()


# Fitting a regression model
linreg = LinearRegression()
linreg.fit(X_rooms, y)

pred_space = np.linspace(min(X_rooms), max(X_rooms)).reshape(-1, 1)

fig, ax = plt.subplots()
ax.scatter(X_rooms, y, s=3, color="blue")
ax.plot(pred_space, linreg.predict(pred_space), color="red", linewidth=3)
ax.set(
    title="Boston Housing Data",
    xlabel="Value of House per 1000($)",
    ylabel="Number of Rooms",
)
plt.show()


# split the data into train and test sets
splits = train_test_split(X, y, test_size=0.3, random_state=42)
X_train, X_test, y_train, y_test = splits

# all features
linreg = LinearRegression()
linreg.fit(X_train, y_train)
y_pred = linreg.predict(X_test)

# R squared
print(f"R-squared: {linreg.score(X_test, y_test)}")

# gapminder data
# ===============
gapminder = pd.read_csv(data_file + "gm_2008_region.csv")
print(f"\n{gapminder.head()}")

sns.heatmap(gapminder.corr(), square=True, cmap="RdYlGn")
plt.show()

# fertility vs life
X = gapminder.fertility.values
y = gapminder.life.values

print(f"\nshape X: {X.shape}\nshape y: {y.shape}")

# reshape
X = X.reshape(-1, 1)
y = y.reshape(-1, 1)
print(f"\nshape X: {X.shape}\nshape y: {y.shape}")

# Visualize the relationship between fertility and life
fig, ax = plt.subplots()
ax.scatter(X, y, s=3)
ax.set(
    title="Relationship between fertility and life expectency",
    xlabel="fertility",
    ylabel="life Expectency",
)
plt.show()

lin_reg = LinearRegression()
lin_reg.fit(X, y)
print(f"R-squared: {lin_reg.score(X, y)}")

prediction_space = np.linspace(X.min(), X.max()).reshape(-1, 1)

# regression line
fig, ax = plt.subplots()
ax.scatter(X, y, s=3, color="blue")
ax.plot(prediction_space, lin_reg.predict(prediction_space), color="red", linewidth=3)
ax.set(
    title="Relationship between fertility and life expectency",
    xlabel="fertility",
    ylabel="life Expectency",
)
plt.show()

# taking all features
X = gapminder.drop(["life", "Region"], axis=1).values
y = gapminder.life.values
y = y.reshape(-1, 1)
print(X.shape)
print(y.shape)

# split
splits = train_test_split(X, y, test_size=0.3, random_state=42)
X_train, X_test, y_train, y_test = splits
lin_reg.fit(X_train, y_train)
y_pred = lin_reg.predict(X_test)

print(f"R-squared: {lin_reg.score(X_test, y_test)}")

rmse = np.sqrt(mean_squared_error(y_test, y_pred))
print(f"Root Mean Squared Error: {rmse}")

# Cross-Validation

cv_results = cross_val_score(lin_reg, X, y, cv=5)
print(f"cv results:\n{cv_results}")

# Regularized regression
# Ridge Regression

ridge = Ridge(alpha=0.1, normalize=True)
ridge.fit(X_train, y_train)
ridge_pred = ridge.predict(X_test)

print(f"Ridge Score: {ridge.score(X_test, y_test)}")

fig, ax = plt.subplots()
alphas = np.linspace(0.1, 0.9, num=9)
rsl = []
for i in alphas:
    ridge = Ridge(alpha=i, normalize=True)
    ridge.fit(X_train, y_train)
    rs = ridge.score(X_test, y_test)
    rsl.append(rs)
ax.plot(alphas, rsl)
ax.set(xlabel="alpha (Ridge)", ylabel="R-squared")
plt.show()

# Lasso Regression
# can be used to select important features of a dataset
# it shrinks the coefficient of less important features to exactly 0

lasso = Lasso(alpha=0.3, normalize=True)
lasso_coef = lasso.fit(X, y).coef_

names = gapminder.drop(["life", "Region"], axis=1).columns

fig, ax = plt.subplots()
ax.plot(range(len(names)), lasso_coef)
ax.set_xticks(range(len(names)))
ax.set_xticklabels(names)
plt.setp(ax.get_xticklabels(), rotation=60)
ax.set_ylabel("Coefficients")
plt.show()

# Regularization II: Ridge
# fitting ridge regression models over a range of different alphas, and plot
# cross-validated R2 scores for each


def display_plot(cv_scores, cv_scores_std):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(alpha_space, cv_scores)

    std_error = cv_scores_std / np.sqrt(10)

    ax.fill_between(
        alpha_space, cv_scores + std_error, cv_scores - std_error, alpha=0.2
    )
    ax.set_ylabel("CV Score +/- Std Error")
    ax.set_xlabel("Alpha")
    ax.axhline(np.max(cv_scores), linestyle="--", color=".5")
    ax.set_xlim([alpha_space[0], alpha_space[-1]])
    ax.set_xscale("log")
    plt.show()


# Setup the array of alphas and lists to store scores
alpha_space = np.logspace(-4, 0, 50)
ridge_scores = []
ridge_scores_std = []

# Create a ridge regressor: ridge
ridge = Ridge(normalize=True)

# Compute scores over range of alphas
for alpha in alpha_space:

    # Specify the alpha value to use: ridge.alpha
    ridge.alpha = alpha

    # Perform 10-fold CV: ridge_cv_scores
    ridge_cv_scores = cross_val_score(ridge, X, y, cv=10)

    # Append the mean of ridge_cv_scores to ridge_scores
    ridge_scores.append(np.mean(ridge_cv_scores))

    # Append the std of ridge_cv_scores to ridge_scores_std
    ridge_scores_std.append(np.std(ridge_cv_scores))

# Display the plot
display_plot(ridge_scores, ridge_scores_std)
