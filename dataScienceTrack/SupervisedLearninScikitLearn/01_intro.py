import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

sns.set()

data_file = "../datasets/"

# load dataset
iris = datasets.load_iris()
print(type(iris))

# keys
print(iris.keys())

# target and features data type
print(f"features type: {type(iris.data)}")
print(f"target type: {type(iris.target)}")

# dimensions of data
print(iris.data.shape)

# feature names
print(iris.feature_names)

# target names
print(iris.target_names)

# EDA

X = iris.data
y = iris.target

df = pd.DataFrame(X, columns=iris.feature_names)
print(f"\n{df.head()}")

pd.plotting.scatter_matrix(df, c=y, figsize=[8, 12], s=50, marker="D")
plt.show()

species = pd.Series(y, dtype="category").cat.rename_categories(
    {0: "setosa", 1: "versicolor", 2: "virginica"}
)
df["species"] = species
print(f"\n{df.head()}")

# sns matrix
sns.pairplot(df, hue="species")
plt.show()

# US HOuse Votes Data 84

col_names = [
    "party",
    "infants",
    "water",
    "budget",
    "physician",
    "salvador",
    "religious",
    "satellite",
    "aid",
    "missile",
    "immigration",
    "synfuels",
    "education",
    "superfund",
    "crime",
    "duty_free_exports",
    "eaa_rsa",
]

house_votes = pd.read_csv(
    data_file + "house-votes-84.csv", names=col_names, dtype="category"
)
for i in range(1, house_votes.shape[1]):
    house_votes[col_names[i]] = house_votes[col_names[i]].cat.rename_categories(
        {"y": 1, "n": 0, "?": 2}
    )
    house_votes[col_names[i]].cat.as_unordered()

print(f"\n{house_votes.info()}")
print(f"\n{house_votes.head()}")

# visual EDA
# voting pattern for education bill
plt.figure()
sns.countplot(x="education", hue="party", data=house_votes)
plt.xticks([0, 1, 2], ["Abstain", "No", "Yes"])
plt.title("Voting Pattern on Education Bills: US House '84")
plt.show()

# voting pattern for satellite bill
plt.figure()
sns.countplot(x="satellite", hue="party", data=house_votes)
plt.xticks([0, 1, 2], ["Abstain", "No", "Yes"])
plt.title("Voting Pattern on Satellite Bills: US House '84")
plt.show()

# voting pattern for missile bill
plt.figure()
sns.countplot(x="missile", hue="party", data=house_votes)
plt.xticks([0, 1, 2], ["Abstain", "No", "Yes"])
plt.title("Voting Pattern on Missile Bills: US House '84")
plt.show()

# Using scikit-learn to fit a classifier

knn = KNeighborsClassifier(n_neighbors=6)
knn.fit(X, y)

# Predict
X_new = np.array([[5.6, 2.8, 3.9, 1.1], [5.7, 2.6, 3.8, 1.3], [4.7, 3.2, 1.3, 0.2]])
print(X_new.shape)

preds = knn.predict(X_new)
print(preds)

# house_votes
y = house_votes["party"].values
X = house_votes.drop("party", axis=1).values
knn = KNeighborsClassifier(n_neighbors=6)
knn.fit(X, y)

X_new = np.random.rand(1, 16)
X_new = np.where(X_new > 0.5, 1, 0)
print(f"prediction: {knn.predict(X_new)}")

# split the data
splits = train_test_split(X, y, test_size=0.3, random_state=5771, stratify=y)
X_train, X_test, y_train, y_test = splits


knn = KNeighborsClassifier(n_neighbors=8)
knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)
print(knn.score(X_test, y_test))

# digits datasets
digits = datasets.load_digits()
print(digits.keys())
print(digits["DESCR"])

# shape of the images and data
print(digits.images.shape)
print(digits.data.shape)

# display digit 1010
plt.imshow(digits.images[1010], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()

X = digits.data
y = digits.target

splits = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)
X_train, X_test, y_train, y_test = splits

knn = KNeighborsClassifier(n_neighbors=7)
knn.fit(X_train, y_train)

print(knn.score(X_test, y_test))

# Overfitting & Underfitting

neighbors = np.arange(1, 9)
train_accuracy = np.empty(len(neighbors))
test_accuracy = np.empty(len(neighbors))

for i, k in enumerate(neighbors):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    train_accuracy[i] = knn.score(X_train, y_train)
    test_accuracy[i] = knn.score(X_test, y_test)

# plot
fig, ax = plt.subplots()
ax.plot(neighbors, train_accuracy, label="Training Accuracy")
ax.plot(neighbors, test_accuracy, label="Testing Accuracy")
ax.set(
    title="kNN: Varying number of neighbors",
    xlabel="Number of neighbors",
    ylabel="Accuracy",
)
ax.legend()
plt.show()
