---
title: "Geometries"
author: "Arun Kr. Khattri"
date: "`r Sys.Date()`"
output: 
  pdf_document:
    toc: yes
  html_document:
    fg_height: 5
    fig_width: 8
    highlight: tango
    number_sections: no
    theme: cerulean
    toc: yes
---
<style type = "text/css">
body{ /* Normal */
  font-size: 14px;}
h1{ /* Header 1 */
  font-size: 24px;}
h2{ /* Header 2 */
  font-size: 20px;}
h3{ /* Header 3 */
  font-size: 16px;}
code.r{ /* Code Block */
  font-size: 12px;}
pre{ /* Code Block */
  font-size: 12px;}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Scatter Plots
```{r message=FALSE}
library(tidyverse)
```

loading iris dataset

```{r}
data(iris)
glimpse(iris)
```

there are around 48 geometries...

Each geom can accept specific aesthetic mappings, e.g. geom_point()

essential: x, y

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width)) +
  geom_point()
```
optional:
alpha, color, fill, shape, size, stroke

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width,
             col=Species)) +
  geom_point()
```
```{r}
iris_summary <- iris %>% 
  group_by(Species) %>% 
  summarise_all(mean)

iris_summary
```
plot the summary statistics on top of the earlier scatter plot

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width,
             col=Species)) +
  geom_point() +
  geom_point(data = iris_summary, shape=8, size=5)
```

playing with shapes

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width,
             col=Species)) +
  geom_point() +
  geom_point(data = iris_summary, shape=21, size=5,
             fill='black', stroke=2)
```
### To deal with over plotting

in case:
* Large datasets
* Aligned values on a single axis.

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width,
             col=Species)) +
  geom_jitter(alpha=0.5)

```
another way is to change the shape = 1 which is a hollow circle

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width,
             col=Species)) +
  geom_jitter(shape=1)

```


### loading diamond dataset

```{r}
data("diamonds")
glimpse(diamonds)
```

plotting carat vs price

```{r}
diamonds %>% 
  ggplot(aes(carat, price, color=clarity)) +
  geom_point(alpha=0.5, shape=".", size=1)
```

updating the shape

```{r}
diamonds %>% 
  ggplot(aes(carat, price, color=clarity)) +
  geom_point(alpha=0.5, shape=16)
```


some more examples of overplotting

```{r}
data("mtcars")
glimpse(mtcars)
```

plot cyl vs mpg

```{r}
mtcars %>% 
  ggplot(aes(factor(cyl), mpg, color=factor(am))) +
  geom_point(position = position_jitter(width = 0.3))
```
alternatively using `position_jitterdodge()`

```{r}
mtcars %>% 
  ggplot(aes(factor(cyl), mpg, color=factor(am))) +
  geom_point(position = position_jitterdodge(jitter.width = 0.3,
                                             dodge.width = 0.3))
```

#### another case when Low-precision data

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width, color=Species)) +
  geom_point(position=position_jitter(0.1))
```

