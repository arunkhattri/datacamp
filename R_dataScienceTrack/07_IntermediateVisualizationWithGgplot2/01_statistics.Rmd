---
title: "Statistics"
author: "Arun Kr. Khattri"
date: "`r Sys.Date()`"
output: 
  html_document:
    fg_height: 5
    fig_width: 8
    highlight: tango
    number_sections: no
    theme: cerulean
    toc: yes
  pdf_document:
    toc: yes
---
<style type = "text/css">
body{ /* Normal */
  font-size: 14px;}
h1{ /* Header 1 */
  font-size: 24px;}
h2{ /* Header 2 */
  font-size: 20px;}
h3{ /* Header 3 */
  font-size: 16px;}
code.r{ /* Code Block */
  font-size: 12px;}
pre{ /* Code Block */
  font-size: 12px;}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Statistics

begin with `_stats`

The geom_/stat_ connection

----------------------------------------------------
stat_            geom_
----------------------------------------------------
`stat_bin()     `geom_histogram()`, `geom_freqpoly()`
`stat_count()   `geom_bar()`
----------------------------------------------------

## libraries

```{r}
library(tidyverse)
library(car)
```

# stat_smooth()

```{r }
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width, color = Species)) +
  geom_point() +
  geom_smooth()
```

the gray ribbon behind our smooth is standard error, is by default, a 95% confidence interval.

standard error can be removed...

```{r }
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width, color = Species)) +
  geom_point() +
  geom_smooth(se=FALSE)
```

method = "lm"

```{r }
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width, color = Species)) +
  geom_point() +
  geom_smooth(method = "lm", se=FALSE)
```

Notice that in both the LOESS and LM examples, the model is calculated on groups defined by color.


### Exercise: Smoothing

* Look at the structure of mtcars.

```{r}
glimpse(mtcars)
```

* Using mtcars, draw a scatter plot of mpg vs. wt.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg)) +
  geom_point()
```

* Update the plot to add a smooth trend line. Use the default method, which uses the LOESS model to fit the curve.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg)) +
  geom_point() +
  geom_smooth()
```

* Update the smooth layer. Apply a linear model by setting method to "lm", and turn off the model's 95% confidence interval (the ribbon) by setting se to FALSE.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE)

```

* Draw the same plot again, swapping geom_smooth() for stat_smooth().

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg)) +
  geom_point() +
  stat_smooth(method = "lm", se = FALSE)

```

### Exercise: Grouping Variables

* Using mtcars, plot mpg vs. wt, colored by fcyl.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg, color = factor(cyl))) +
  geom_point()
```

* Add a smooth stat using a linear model, and don't show the se ribbon.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg, color = factor(cyl))) +
  geom_point() +
  stat_smooth(method = "lm", se = FALSE)
```

* Update the plot to add a second smooth stat.
  + Add a dummy group aesthetic to this layer, setting the value to 1.
  + Use the same method and se values as the first stat smooth layer.
  
```{r}
mtcars %>% 
  ggplot(aes(wt, mpg, color = factor(cyl))) +
  geom_point() +
  stat_smooth(method = "lm", se = FALSE) +
  stat_smooth(aes(group = 1), method = "lm", se = FALSE)
```
  
### Exercise: Modifying stat_smooth

In the previous exercise we used se = FALSE in stat_smooth() to remove the 95% Confidence Interval. Here we'll consider another argument, span, used in LOESS smoothing, and we'll take a look at a nice scenario of properly mapping different models.
  
* Explore the effect of the span argument on LOESS curves. Add three smooth LOESS stats, each without the standard error ribbon.

  + Color the 1st one "red"; set its span to 0.9.
  + Color the 2nd one "green"; set its span to 0.6.
  + Color the 3rd one "blue"; set its span to 0.3.

```{r}
mtcars %>% 
  ggplot(aes(wt, mpg)) +
  geom_point() +
  stat_smooth(se = FALSE, color = "red", span = 0.9) +
  stat_smooth(se = FALSE, color = "green", span = 0.6) +
  stat_smooth(se = FALSE, color = "blue", span = 0.3)
```

* Compare LOESS and linear regression smoothing on small regions of data.

  + Add a smooth LOESS stat, without the standard error ribbon.
  + Add a smooth linear regression stat, again without the standard error ribbon.
  
```{r}
mtcars %>% 
  ggplot(aes(wt, mpg, color = factor(cyl))) +
  geom_point() +
  stat_smooth(se = FALSE) +
  stat_smooth(se = FALSE, method = "lm")
```
  
* LOESS isn't great on very short sections of data; compare the pieces of linear regression to LOESS over the whole thing.

  + Amend the smooth LOESS stat to map color to a dummy variable, "All".
  
```{r}
mtcars %>% 
  ggplot(aes(wt, mpg, color = factor(cyl))) +
  geom_point() +
  stat_smooth(aes(color = "All"), se = FALSE) +
  stat_smooth(method = "lm", se = FALSE)
```


### Exercise: Modifying stat_smooth (2)

In this exercise we'll take a look at the standard error ribbons, which show the 95% confidence interval of smoothing models. ggplot2 and the Vocab data frame are already loaded for you.

Vocab has been given an extra column, year_group, splitting the dates into before and after 1995.

```{r}
vocab <- Vocab %>% 
  mutate(year_group = factor(ifelse(year <= 1995,
                                    "[1974, 1995]", "(1995, 2016]"),
                             levels = c("[1974, 1995]", "(1995, 2016]")))

glimpse(vocab)
```

* Using Vocab, plot vocabulary vs. education, colored by year_group.

* Use geom_jitter() to add jittered points with transparency 0.25.

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary, color = year_group)) +
  geom_jitter(alpha = 0.25)
```

* Add a smooth linear regression stat (with the standard error ribbon).

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary, color = year_group)) +
  geom_jitter(alpha = 0.25) +
  stat_smooth(method = "lm")

```

It's easier to read the plot if the standard error ribbons match the lines, and the lines have more emphasis.

* Update the smooth stat.
  + Map the fill color to year_group.
  + Set the line size to 2.

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary, color = year_group)) +
  geom_jitter(alpha = 0.25) +
  stat_smooth(aes(fill = year_group), method = "lm", size= 2)
```


## Stats: sum and quantile

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width)) +
  geom_point()
```

Jittering may give a wrong impressions that we have more precision than we actually do

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width)) +
  geom_jitter(alpha = 0.5,
              width = 0.1,
              height = 0.1)
```

We should always mention that we've jittered our data because of this.
To avoid this , we can use another variant, `geom_count`.
geom_count counts the number of observations at each location and then maps the count onto size as the point area.

```{r}
iris %>% 
  ggplot(aes(Sepal.Length, Sepal.Width)) +
  geom_count()
```

## Dealing with **heteroscedesticity**

refers to the circumstance in which the variability of a variable is unequal across the range of values of a second variable that predicts it.
Ref: [Heteroscedesticity explained](https://statisticsbyjim.com/regression/heteroscedasticity-regression/)

```{r}
library(AER)
data(Journals)

p <- Journals %>% 
  ggplot(aes(log(price/citations), log(subs))) +
  geom_point(alpha = 0.5) +
  labs(x = "Library price relative to total number of citations (log)",
       y = "Number of library subscriptions (log)",
       caption = "Economics Journal subscription data, year 2000")

p
```

we can see that variance on the y axis is not consistent as we move along x axis.

Using `geom_quantile` to model the 5th and the 95th percentile as well as the median, the 50th percentile.

```{r}
p +
  geom_quantile(quantiles = c(0.05, 0.50, 0.95))
```

### Exercise: Quantiles

Here, we'll continue with the `Vocab` dataset and use `stat_quantile()` to apply a quantile regression.

Linear regression predicts the mean response from the explanatory variables, quantile regression predicts a quantile response (e.g. the median) from the explanatory variables. Specific quantiles can be specified with the quantiles argument.

```{r}
vp <- vocab %>% 
  ggplot(aes(education, vocabulary)) +
  geom_jitter(alpha = 0.25)

vp
```

* Update the plot to add a quantile regression stat, at quantiles 0.05, 0.5, and 0.95.

```{r}
vp +
  geom_quantile(quantiles = c(0.05, 0.5, 0.95))
```

* amend the plot to color by year_group

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary, color = year_group)) +
  geom_jitter(alpha = 0.25) +
  geom_quantile(quantiles = c(0.05, 0.5, 0.95))
```

### Using stat_sum

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary)) +
  stat_sum()
```

Modify the size aesthetic with the appropriate scale function.

Add a scale_size() function to set the range from 1 to 10.

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary)) +
  stat_sum() +
  scale_size(range = c(1, 10))
```


Inside stat_sum(), set size to ..prop.. so circle size represents the proportion of the whole dataset.

```{r}
vocab %>% 
  ggplot(aes(education, vocabulary)) +
  stat_sum(aes(size = ..prop..)) 
```

Update the plot to group by education, so that circle size represents the proportion of the group.


```{r}
vocab %>% 
  ggplot(aes(education, vocabulary, group = education)) +
  stat_sum(aes(size = ..prop..)) 

```


## Stats outside geoms

```{r}
library(Hmisc)

```
calculating statistics

```{r}
set.seed(123)
xx <- rnorm(100)
smean.sdl(xx, mult = 1)
```

ggplot2

```{r}
mean_sdl(xx, mult = 1)
```

stat_summary()

```{r}
iris %>% 
  ggplot(aes(Species, Sepal.Length)) +
  stat_summary(fun.data = mean_sdl,
               fun.args = list(mult = 1))
```

set the width of the error

```{r}
iris %>% 
  ggplot(aes(Species, Sepal.Length)) +
  stat_summary(fun.y = mean,
               geom = "point") +
  stat_summary(fun.data = mean_sdl,
               fun.args = list(mult = 1),
               geom = "errorbar",
               width = 0.1)

```

### Exercise

Preparations

```{r}
# define position objects; jitter with width 0.2
posn_j <- position_jitter(width = 0.2)

# dodge with width 0.1
posn_d <- position_dodge(width = 0.1)

# jitter-dodge with jitter.width 0.2 and dodge.width 0.1
posn_jd <- position_jitterdodge(jitter.width = 0.2, 
                                dodge.width = 0.1)

# plot wt vs factor(cyl) colored by factor(am)
base_plot <- mtcars %>% 
  ggplot(aes(factor(cyl), wt, color = factor(am)))

base_plot +
  geom_point()
```

Applying jitter only

```{r}
base_plot +
  geom_point(position = posn_j)
```

Applying dodge only

```{r}
base_plot +
  geom_point(position = posn_d)
```

Applying position-jitter

```{r}
base_plot_jd <- base_plot +
  geom_point(position = posn_jd)

base_plot_jd
```

#### Plotting Variations

* Add error bars representing the standard deviation.
  * Set the data function to mean_sdl
  * Draw 1 std each side of the mean
  * use posn_d to set the position

```{r}
base_plot_jd +
  stat_summary(fun.data = mean_sdl,
               fun.args = list(mult = 1),
               position = posn_d)
```

use "errorbar" geom

```{r}
base_plot_jd +
  stat_summary(fun.data = mean_sdl,
               fun.args = list(mult = 1),
               position = posn_d,
               geom = "errorbar")
```

adding a summary stat of 95% confidence limits

```{r}
base_plot_jd +
  stat_summary(fun.data = mean_cl_normal,
               position = posn_d)
```


