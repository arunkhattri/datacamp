---
title: "Full, Semi, and Anti Joins"
author: "Arun Kr. Khattri"
date: "22/09/2020"
output:
  pdf_document:
    toc: yes
  html_document:
    fg_height: 5
    fig_width: 8
    highlight: tango
    number_sections: no
    theme: cerulean
    toc: yes
---
<style type = "text/css">
body{ /* Normal */
  font-size: 14px;
}
h1{ /* Header 1 */
  font-size: 24px;
}
h2{ /* Header 2 */
  font-size: 20px;
}
h3{ /* Header 3 */
  font-size: 16px;
}
code.r{ /* Code Block */
  font-size: 12px;
}
pre{ /* Code Block */
  font-size: 12px;
}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## libraries
```{r, message=FALSE}
library(tidyverse)
library(forcats)
```

## datasets related to lego

```{r}
sets <- readRDS("./data/sets.rds")
themes <- readRDS("./data/themes.rds")
inventories <- readRDS("./data/inventories.rds")
parts <- readRDS("./data/parts.rds")
inventory_part <- readRDS("./data/inventory_parts.rds")
colors <- readRDS("./data/colors.rds")
```

### Joining and filtering

```{r}
inventory_parts_joined <- inventories %>% 
  inner_join(inventory_part,
             by = c("id" = "inventory_id")) %>% 
  arrange(desc(quantity)) %>% 
  select(-id, -version)

# batmobile
batmobile <- inventory_parts_joined %>% 
  filter(set_num == "7784-1") %>% 
  select(-set_num)

# batwing
batwing <- inventory_parts_joined %>% 
  filter(set_num == "70916-1") %>% 
  select(-set_num)
```

## full_join verb

keep all observations from both left and right table

```{r}
bat_joined <- batmobile %>% 
  full_join(batwing,
            by = c("part_num", "color_id"),
            suffix = c("_batmobile", "_batwing"))

bat_joined
```

this time quantity_batmobile and quantity_batwing both will contain NA's

```{r}
# count of NA's
bat_joined %>% 
  sapply(function(x) sum(is.na(x)))
```

here NA implies 0, replace it with Zeroes...

```{r}
bat_joined %>% 
  replace_na(list(quantity_batmobile = 0,
                  quantity_batwing = 0)) %>% 
  tail(10)
```

### Differences between Batman and Start Wars

Joining `inventory_parts_joined`, `sets` and `themes` together

```{r}
inv_set_theme <- inventory_parts_joined %>% 
  inner_join(sets, by = 'set_num') %>% 
  inner_join(themes,
             by = c("theme_id" = "id"),
             suffix = c("_set", "_theme"))
 inv_set_theme
  
```

### Aggregating each theme

Batman

```{r}
batman <- inv_set_theme %>% 
  filter(name_theme == "Batman")

batman_parts <- batman %>% 
  count(part_num, color_id, wt=quantity)

batman_parts
```

Star Wars

```{r}
star_wars <- inv_set_theme %>% 
  filter(name_theme == "Star Wars")

star_wars_parts <- star_wars %>% 
  count(part_num, color_id, wt=quantity)

star_wars_parts
```

Combining Batman and Star Wars themes...

```{r}
combined <- batman_parts %>% 
  full_join(star_wars_parts,
            by = c("part_num", "color_id"),
            suffix = c("_batman", "_star_wars")) %>% 
  replace_na(list(n_batman = 0,
                  n_star_wars = 0))

glimpse(combined)
```


```{r}
```

However, we have more information about each of these parts that we can gain by combining this table with some of the information we have in other tables. Before we compare the themes, let's ensure that we have enough information to make our findings more interpretable.

```{r}
# sort the star wars pieces in descending order
combined_color_parts <- combined %>% 
  arrange(desc(n_star_wars)) %>% 
  # join colors table
  left_join(colors, by = c("color_id" = "id")) %>% 
  # join parts table
  left_join(parts, by='part_num', suffix = c("_color", "_part"))

glimpse(combined_color_parts)
```

## The Semi and Anti Join Verbs

Filtering joins
+ *Semi Join* : What observations in X are also in Y?
+ *Anti Join* : What observations in X are *not* in Y?

### semi-join

what parts used in the Batmobile set are also used in the Batwing set ?

```{r}
batmobile %>% 
  semi_join(batwing, by = c("color_id", "part_num"))
```

45 pieces...used in both batmobile and batwing.

Which parts of batmobile are not in batwing

```{r}
batmobile %>% 
  anti_join(batwing, by=c("color_id", "part_num"))
```

128 parts... not in batwing.

### Visualizing set difference

aggregating sets into colors

```{r}
batmobile_colors <- batmobile %>% 
  group_by(color_id) %>% 
  summarize(total = sum(quantity))

batmobile_colors
```
for batwing

```{r}
batwing_colors <- batwing %>% 
  group_by(color_id) %>% 
  summarize(total = sum(quantity))

batwing_colors
```
Comparing color schemes of sets

```{r}
colors_bat <- batmobile_colors %>% 
  full_join(batwing_colors, by='color_id', suffix = c('_bm', '_bw')) %>% 
  replace_na(list(total_bm = 0,
                  total_bw = 0))

colors_bat
```

Adding the color names

```{r}
colors_bat <- colors_bat %>% 
  inner_join(colors, by = c("color_id" = "id"))

colors_bat
```

Adding percentages

```{r}
colors_bat_agg <- colors_bat %>% 
  mutate(total_bm = total_bm / sum(total_bm),
         total_bw = total_bw / sum(total_bw),
         difference = total_bm - total_bw)

colors_bat_agg
```

functions for visualization

```{r}
color_palette <- setNames(colors_bat_agg$rgb, colors_bat_agg$name)

colors_bat_agg %>% 
  mutate(name = fct_reorder(name, difference)) %>% 
  ggplot(aes(name, difference, fill=name)) +
  geom_col() +
  coord_flip() +
  scale_fill_manual(values = color_palette , guide = FALSE)
```

colors on the right of 0 are more common in batmobile and left of 0 has colors more common in batwing...