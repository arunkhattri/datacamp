"""
VaR provides an estimate, under a given degree of confidence, of the size of a
loss from a portfolio over a given time period.
"""
import yfinance as yf
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

# Historical drawdown

TICKER = "BANDHANBNK.NS"

bb = yf.download(TICKER, period="1y")
print(bb.info())

plt.plot(bb.index, bb["Adj Close"])
plt.ylabel("Close Price")
plt.title("Bandhan Bank")
plt.show()


# stock return
bb["returns"] = np.log(bb["Adj Close"] / bb["Adj Close"].shift(1))
print(bb.loc[:, ["Adj Close", "returns"]].head())
# drop na
bb.dropna(inplace=True)

plt.hist(bb.returns, bins=50)
plt.show()

plt.plot(bb.index, bb.returns)
plt.show()


# cumulative returns
bb["cum_rets"] = np.cumsum(bb["returns"]) + 1
print(bb.loc[:, ["Adj Close", "returns", "cum_rets"]].head())

plt.plot(bb.index, bb.cum_rets)
plt.ylabel("cumulative returns")
plt.title("Bandhan Bank Cumulative Returns")
plt.show()

cum_rets = bb.cum_rets
running_max = np.maximum.accumulate(cum_rets)
running_max[running_max < 1] = 1
drawdown = (cum_rets / running_max) - 1
print(drawdown[:5])

# visualize
drawdown.plot()
plt.title("Historical drawdown")
plt.show()

# Historical Value at Risk
# threshold with a fiven confidence level that losses will not(or more
# accurately, will not historically) exceed a certain level
# VaR is commonly quoted with quantiles such as 95, 99 and 99.9
var_level = 95
var_95 = np.percentile(bb.returns, 100 - var_level)
print(var_95)

# sort the returns for plotting
sorted_rets = sorted(bb.returns)
plt.hist(sorted_rets, density=True, bins=50)
plt.axvline(x=var_95, color="r", linestyle="-", label=f"VaR95:{var_95:.2f}%")
plt.show()

# Historical expected shortfall
cvar95 = bb.returns[bb.returns <= var_95].mean()
# print(cvar95)

plt.hist(sorted_rets, density=True, bins=50)
plt.axvline(x=var_95, color="r", linestyle="-", label=f"VaR95:{var_95:.2f}%")
plt.axvline(x=cvar95, color="g", linestyle="-", label=f"cvar95:{cvar95:.2f}%")
plt.legend()
plt.show()

var_99 = np.percentile(bb.returns, 100 - 99)
cvar99 = bb.returns[bb.returns <= var_99].mean()

plt.hist(sorted_rets, density=True, bins=50)
plt.axvline(x=var_95, color="r", linestyle="-", label=f"VaR95:{var_95:.2f}%")
plt.axvline(x=cvar95, color="g", linestyle=":", label=f"cvar95:{cvar95:.2f}%")
plt.axvline(x=var_99, color="c", linestyle="-", label=f"VaR99:{var_99:.2f}%")
plt.axvline(x=cvar99, color="b", linestyle=":", label=f"cvar99:{cvar99:.2f}%")
plt.legend()
plt.show()

# Parametric VaR
mu = np.mean(bb.returns)
sigma = np.std(bb.returns)
conf_level = 0.05
VaR = norm.ppf(conf_level, mu, sigma)
print(f"VaR: {VaR}")

# forecast
forecast_days = 5
forecast_var95_5day = VaR * np.sqrt(forecast_days)
print(f"forecast for 5 day VaR: {forecast_var95_5day}")

# 21 day forecast
forecast_val = np.empty([21, 2])
for i in range(21):
    forecast_val[i, 0] = i
    forecast_val[i, 1] = VaR * np.sqrt(i + 1)


def plot_VaR(forecast_val):
    """Plot the forecast vs time"""
    plt.plot(forecast_val[:, 0], -1 * forecast_val[:, 1])
    plt.xlabel("Time Horizon T + 1")
    plt.ylabel("Forecasted VaR95 (%)")
    plt.title("VaR 95 scaled by Time")
    plt.show()


plot_VaR(forecast_val)


# Random Walks in Python
def random_walk(mu, sigma, S0, T=252):
    """
    Random Walk of a ticker

    Parameter
    ---------
    mu: float, mean of the ticker's returns
    sigma: float, std deviation of the ticker's returns
    T: int, trading days
    S0: float, starting value

    Return
    ------
    graph, random walk of ticker
    """

    rand_rets = np.random.normal(mu, sigma, T) + 1
    forecasted_values = S0 * (rand_rets.cumprod())
    # print(f"{forecasted_values}")

    plt.plot(range(T), forecasted_values, label="Random Walk")
    plt.plot(range(T), bb["Adj Close"], label="Adj Close")
    plt.legend()
    plt.show()


mu = np.mean(bb.returns)
sigma = np.std(bb.returns)
T = 250
S0 = 240.95
random_walk(mu=mu, sigma=sigma, T=T, S0=S0)


# Monte Carlo Simulation
def monte_carlo_simulation(mu, sigma, S0, T=252, nums=100):
    """
    Monte Carlo Simulation of a ticker.

    Parameter
    ---------
    mu: float, mean of the ticker's returns
    sigma: float, std deviation of the ticker's returns
    T: int, trading days
    S0: float, starting value
    num: int, number of simulations

    Return
    ------
    graph, monte carlo simulation
    """
    for i in range(nums):
        rand_rets = np.random.normal(mu, sigma, T) + 1
        forecasted_values = S0 * (rand_rets.cumprod())
        plt.plot(range(T), forecasted_values)
    plt.show()


mu = np.mean(bb.returns)
sigma = np.std(bb.returns)
T = 249
S0 = 240.95
monte_carlo_simulation(mu=mu, sigma=sigma, T=T, S0=S0)


# Monte Carlo VaR
def monte_carlo_var(mu, sigma, S0, T=252, nums=100):
    """
    Calculate parametric monte carlo value at risk.

    Parameter
    ---------
    mu: float, mean of the ticker's returns
    sigma: float, std deviation of the ticker's returns
    T: int, trading days
    S0: float, starting value
    num: int, number of simulations

    Return
    ------
    Parametric value at risk
    """
    simulation_rets = []

    for i in range(nums):
        rand_rets = np.random.normal(mu, sigma, T)
        simulation_rets.append(rand_rets)
    sim_var_99 = np.percentile(simulation_rets, 1)
    print(f"Parametric VaR(99): {100 * sim_var_99:.2f}%")


mu = np.mean(bb.returns)
sigma = np.std(bb.returns)
T = 249
S0 = 240.95
monte_carlo_var(mu, sigma, S0, T)

