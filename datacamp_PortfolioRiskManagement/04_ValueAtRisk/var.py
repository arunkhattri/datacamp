import numpy as np
from scipy.stats import norm
import yfinance as yf


def var_cov_var(P, c, mu, sigma):
    """
    Variance-Covariance calculation of daily Value-at-Risk
    using confidence level c, with mean of returns mu
    and standard deviation of returns sigma, on a portfolio
    of value P.
    """
    alpha = norm.ppf(1-c, mu, sigma)
    return P - P*(alpha + 1)


def value_at_risk(ticker, P=10000, c=0.99):
    """
    Calculates Value at Risk for given investment P
    Parameters
    ----------
    ticker: str, stock symbols
    P: float, initial investment
    c: float, confidence interval
    Returns
    -------
    float, Value at Risk
    """

    df = yf.download(ticker, period='2y')

    df["rets"] = df["Adj Close"].pct_change()

    mu = np.mean(df["rets"])
    sigma = np.std(df["rets"])

    var = var_cov_var(P, c, mu, sigma)
    print(f"Value at Risk: {var:.2f}")


if __name__ == "__main__":
    ticker = 'SBIN.NS'
    value_at_risk(ticker)
