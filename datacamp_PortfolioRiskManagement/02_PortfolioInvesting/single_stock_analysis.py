# to be added python financial functions utility
import numpy as np
import yfinance as yf
import empyrical as ep
import scipy.stats as stats


def pretty_print(key_measures, ticker):
    """
    Pretty prints the key measures.

    Parameters
    ----------
    key_measures: dict
        {'measure_name': measure_vals}

    """
    max_len_km = 0
    for km in key_measures.keys():
        if len(km) > max_len_km:
            max_len_km = len(km)

    # calmar: 6.18047
    print("-" * (max_len_km + 11))
    print(f"Key indicators ({ticker}):")
    print("-" * (max_len_km + 11))
    for k, v in key_measures.items():
        print(f"{k:<{max_len_km + 2}}: {v:.4f}")

    print("=" * (max_len_km + 11))


def value_at_risk(returns, period=None, sigma=2.0):
    """
    Get value at risk (VaR).
    Parameters
    ----------
    returns : pd.Series
        Daily returns of the strategy, noncumulative.
         - See full explanation in tears.create_full_tear_sheet.
    period : str, optional
        Period over which to calculate VaR. Set to 'weekly',
        'monthly', or 'yearly', otherwise defaults to period of
        returns (typically daily).
    sigma : float, optional
        Standard deviations of VaR, default 2.
    """
    if period is not None:
        returns_agg = ep.aggregate_returns(returns, period)
    else:
        returns_agg = returns.copy()

    value_at_risk = returns_agg.mean() - sigma * returns_agg.std()
    return value_at_risk


def key_indicators(ticker, benchmark, start, end, period="daily"):
    """
    Calculate key indicators for trading
    Parameters
    ----------
    ticker: str
        symbol of the stock in yahoo
    benchmark: str
        symbol of the benchmark index
    start: str
        format: "yyyy-mm-dd"
    end: str
        format: "yyyy-mm-dd"
    period: str
        values are 'daily', 'weekly' or 'monthly'

    Return
    ------
    Annual Return
    ...
    Beta
    """
    df_info = yf.Ticker(ticker)
    df_bm = yf.Ticker(benchmark)

    # historical data for 1 year
    #  df = df_info.history(yf_period, actions=False)
    #  bm = df_bm.history(yf_period, actions=False)
    df = yf.download(ticker, start=start, end=end)
    bm = yf.download(benchmark, start=start, end=end)

    # calculate daily logarithmic return
    df["returns"] = np.log(df['Adj Close'] / df['Adj Close'].shift(1))
    bm["returns"] = np.log(bm['Adj Close'] / bm['Adj Close'].shift(1))

    stock_rets = df["returns"][1:]
    bm_rets = bm["returns"][1:]

    alpha, beta = ep.alpha_beta(stock_rets, bm_rets)
    ann_rets = ep.annual_return(stock_rets, period)
    calmar = ep.calmar_ratio(stock_rets, period)
    omega = ep.omega_ratio(stock_rets)
    sortino = ep.sortino_ratio(stock_rets)
    max_drawdown = ep.max_drawdown(stock_rets)
    sharp_ratio = ep.sharpe_ratio(stock_rets, risk_free=0)
    stability = ep.stability_of_timeseries(stock_rets)
    tail_ratio = ep.tail_ratio(stock_rets)
    kurtosis = stats.kurtosis(stock_rets)
    skew = stats.skew(stock_rets)
    ann_volatility = ep.annual_volatility(stock_rets)
    var = value_at_risk(stock_rets)
    cum_return = sum(stock_rets)

    km = {
        "Annual return": ann_rets,
        "Annual volatility": ann_volatility,
        "Cumulative Return": cum_return,
        "Sharp ratio": sharp_ratio,
        "Calmar ratio": calmar,
        "Stability": stability,
        "Max drawdown": max_drawdown,
        "Omega ratio": omega,
        "Sortino ratio": sortino,
        "Skew": skew,
        "Kurtosis": kurtosis,
        "Tail ratio": tail_ratio,
        "Daily value at risk": var,
        "Alpha": alpha,
        "Beta": beta,
    }
    return km


if __name__ == "__main__":
    TICKER = "SBIN.NS"
    start = "2019-05-04"
    end = "2021-05-04"
    BENCHMARK = "%5ENSEI"
    km = key_indicators(TICKER, BENCHMARK, start, end)
    pretty_print(km, TICKER[:-3])
