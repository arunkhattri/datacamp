import yfinance as yf
from datetime import date, timedelta
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def volatility_stock(ticker, period):
    """
    Calculate volatility of the given stock for the given time period.

    Parameter
    ---------
    ticker: str; symbol of the stock
    period: str; historical data period

    Return
    ------
    histogram for the volatility
    """
    # get stock ticker symbol from user

    # get data
    df_info = yf.Ticker(ticker)

    # historical data for 1 year
    df = df_info.history(period, actions=False)

    # calculate daily logarithmic return
    df['returns'] = (np.log(df.Close / df.Close.shift(1)))

    # calculate daily standard deviation of returns
    daily_std = np.std(df.returns)

    # annualize daily standard deviation
    std = daily_std * 252 ** 0.5

    # Plot histograms
    fig, ax = plt.subplots(1, 1, figsize=(7, 5))
    n, bins, patches = ax.hist(
        df.returns.values,
        bins=50, alpha=0.65, color='blue',
        label='12-month')

    ax.set_xlabel('log return of stock price')
    ax.set_ylabel('frequency of log return')
    ax.set_title('Historical Volatility for ' + stock_symbol)

    # get x and y coordinate limits
    x_corr = ax.get_xlim()
    y_corr = ax.get_ylim()

    # make room for text
    header = y_corr[1] / 5
    y_corr = (y_corr[0], y_corr[1] + header)
    ax.set_ylim(y_corr[0], y_corr[1])

    # print historical volatility on plot
    x = x_corr[0] + (x_corr[1] - x_corr[0]) / 30
    y = y_corr[1] - (y_corr[1] - y_corr[0]) / 15
    ax.text(x, y, 'Annualized Volatility: ' + str(np.round(std*100, 1)) + '%',
            fontsize=11, fontweight='bold')
    x = x_corr[0] + (x_corr[1] - x_corr[0]) / 15
    y -= (y_corr[1] - y_corr[0]) / 20

    # display plot
    fig.tight_layout()
    #  fig.savefig('historical volatility.png')
    plt.show()


if __name__ == "__main__":
    stock_symbol = 'SAIL.NS'
    period = "2y"
    volatility_stock(stock_symbol, period)
