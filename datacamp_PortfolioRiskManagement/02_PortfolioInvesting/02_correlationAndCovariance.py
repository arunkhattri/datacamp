from datetime import datetime
import pandas as pd
import numpy as np
import pandas_datareader.data as web
import yfinance as yf
import seaborn as sns
import matplotlib.pyplot as plt

TICKER_LIST = [
    "ARVINDFASN.NS",
    "BEL.NS",
    "HINDUNILVR.NS",
    "IDEA.NS",
    "ITC.NS",
    "NBCC.NS",
    "NTPC.NS",
    "SAIL.NS",
    "SUNPHARMA.NS",
    "%5ENSEI",
]
START = datetime(2019, 3, 27)
END = datetime(2021, 4, 15)
PF_CSV = "../../../data/pf.csv"


def pf_wts(pf_csv):
    """
    Calculates portfolio weights

    Parameters
    ----------
    pf_csv: csv; portfolio data containing symbol, Qty, Avg. cost price

    Returns
    -------
    wts: list; symbols wts in portfolio
    """
    pf = pd.read_csv(pf_csv)

    # calculate value at cost
    pf["value_at_cost"] = pf["Qty"] * pf["Average Cost Price"]

    cost = pf["value_at_cost"].sum()
    pf["wt"] = pf["value_at_cost"] / cost

    tickers = pf["Stock Symbol"].values.tolist()
    wts = pf["wt"].values.tolist()
    res = dict(zip(tickers, wts))

    return res


wts = pf_wts(PF_CSV)
print(wts)


def get_returns_pf(ticker_list, start, end):
    """
    Calculate Portfolio returns

    Parameteres
    -----------
    ticker_list: list; of stock symbols
    start: datetime; start date
    end: datetime: end date

    Returns
    -------
    pf_rets: dataframe; portfolio returns
    benchmark_rets: dataframe; benchmark returns
    """
    # get data from yahoo finance

    df = web.DataReader(ticker_list, "yahoo", start=start, end=end)["Adj Close"]

    # rename columns
    df.columns = [
        "ARVINDFASN",
        "BEL",
        "HINDUNILVR",
        "IDEA",
        "ITC",
        "NBCC",
        "NTPC",
        "SAIL",
        "SUNPHARMA",
        "NIFTY",
    ]
    # Drop null values
    df.dropna(inplace=True)

    # portfolio return
    #
    ret_data = df.pct_change()[1:]
    benchmark_ret = df.NIFTY.values[1:]

    return ret_data.iloc[:, :-1], benchmark_ret


pf_rets, benchmark_rets = get_returns_pf(TICKER_LIST, start=START, end=END)

corr_mat = pf_rets.corr()
print(corr_mat)

# plot correlation
sns.set_theme(style="white")
mask = np.triu(np.ones_like(corr_mat, dtype=bool))
f, ax = plt.subplots(figsize=(11, 9))
cmap = sns.diverging_palette(240, 10, as_cmap=True)
sns.heatmap(
    corr_mat,
    annot=True,
    mask=mask,
    cmap=cmap,
    vmax=0.3,
    center=0,
    square=True,
    linewidths=0.5,
    cbar_kws={"shrink": 0.5},
)
plt.show()

# co-variance
cov_mat = pf_rets.cov()
print(cov_mat)

# annualizing the covariance matrix
cov_mat_annual = cov_mat * 252

# Portfolio standard deviation (volatility)
# daily
weights = np.array(list(wts.values()))
port_vol = np.sqrt(np.dot(weights.T, np.dot(cov_mat, weights)))
print(port_vol)
# annual
port_vol_ann = np.sqrt(np.dot(weights.T, np.dot(cov_mat_annual, weights)))
print(port_vol_ann)
