import pandas_datareader.data as web
from datetime import datetime
import numpy as np
import empyrical as ep

end = datetime(2021, 4, 20)
start = datetime(2020, 4, 20)

tickers = ["ICICIBANK.NS", "%5ENSEI"]

df = web.DataReader(tickers, "yahoo", start=start, end=end)["Adj Close"]
df.info()

# yahoo tickers
pf_symbols = [
    "ARVINDFASN.NS",
    "BEL.NS",
    "HINDUNILVR.NS",
    "IDEA.NS",
    "ITC.NS",
    "NBCC.NS",
    "NTPC.NS",
    "SAIL.NS",
    "SUNPHARMA.NS",
    "%5ENSEI",
]

df.columns = ["icici", "nifty"]
print(df.head())

df["icici_rets"] = np.log(df.icici / df.icici.shift(1))
df["nifty_rets"] = np.log(df.nifty / df.nifty.shift(1))

df.dropna(inplace=True)

icici_rets = df["icici_rets"]
nifty_rets = df["nifty_rets"]

alpha, beta = ep.alpha_beta(icici_rets, nifty_rets)
print(f"alpha: {alpha}\nbeta: {beta}")
