import pandas as pd
import numpy as np
import pandas_datareader.data as web
import yfinance as yf
import seaborn as sns
import matplotlib.pyplot as plt

# my portfolio
pf = pd.read_csv("../../data/pf.csv")
print(pf.head())

# calculate value at cost
pf["value_at_cost"] = pf["Qty"] * pf["Average Cost Price"]
print(pf.loc[:, ["Stock Symbol", "Qty", "value_at_cost"]])

cost = pf["value_at_cost"].sum()
print(cost)
pf["wt"] = pf["value_at_cost"] / cost
print(pf["wt"].sum())


# yahoo tickers
pf_symbols = [
    "ARVINDFASN.NS",
    "BEL.NS",
    "HINDUNILVR.NS",
    "IDEA.NS",
    "ITC.NS",
    "NBCC.NS",
    "NTPC.NS",
    "SAIL.NS",
    "SUNPHARMA.NS",
    "%5ENSEI",
]

start = datetime(2018, 3, 27)
end = datetime(2021, 4, 15)
pf_df = web.DataReader(pf_symbols, "yahoo", start=start, end=end)["Adj Close"]
print(pf_df.info())


pf_df.columns = [
    "ARVINDFASN",
    "BEL",
    "HINDUNILVR",
    "IDEA",
    "ITC",
    "NBCC",
    "NTPC",
    "SAIL",
    "SUNPHARMA",
    "NIFTY",
]

# backfill values
pf_df.fillna(method="bfill", inplace=True)
print(pf_df.isna().sum())

# portfolio return
#
ret_data = pf_df.pct_change()[1:]
benchmark_ret = pf_df.NIFTY.values[1:]

wts = pf.wt.values.tolist()

ret_data["pf_ret"] = (ret_data.iloc[:, :-1] * wts).sum(axis=1)

# visualize
sns.regplot(benchmark_ret, ret_data["pf_ret"].values)
plt.xlabel("Nifty returns")
plt.ylabel("Portfolio returns")
plt.title("Portfolio Vs Nifty returns")
plt.show()

# cumulative pf return
ret_data["cum_pf_ret"] = (1 + ret_data["pf_ret"]).cumprod() - 1

# equal weighted portfolio return
num_stocks = len(pf_df.columns) - 1
pf_equal_wts = np.repeat(1 / num_stocks, num_stocks)
ret_data["eq_pf_ret"] = ret_data.iloc[:, :9].mul(pf_equal_wts, axis=1).sum(axis=1)

ret_data["eq_cum_pf_ret"] = (1 + ret_data["eq_pf_ret"]).cumprod() - 1

# visualize
ret_data["eq_cum_pf_ret"].plot(label="Equaly wtd cumulative pf return")
ret_data["cum_pf_ret"].plot(label="cumulative pf return")
plt.legend()
plt.show()

# bel = yf.Ticker('BEL.NS')
# print(bel.info['marketCap'])

# get market capitalization for stocks in pf
mkt_cap = np.zeros(len(pf_symbols[:-1]))
for idx, ticks in enumerate(pf_symbols[:-1]):
    temp = yf.Ticker(ticks)
    mkt_cap[idx] = temp.info["marketCap"]

print(mkt_cap)

mcap_wts = mkt_cap / np.sum(mkt_cap)

print(mcap_wts)
print(np.sum(mcap_wts))

# calculate market cap weighted portfolio returns
ret_data["pf_mcap"] = ret_data.iloc[:, :9].mul(mcap_wts, axis=1).sum(axis=1)

# cumulative market cap weighted portfolio returns
ret_data["pf_mcap_ret"] = (1 + ret_data["pf_mcap"]).cumprod() - 1

# visualize
ret_data["eq_cum_pf_ret"].plot(label="Equaly wtd cumulative pf return")
ret_data["cum_pf_ret"].plot(label="cumulative pf return")
ret_data["pf_mcap_ret"].plot(label="Market cap pf return")
plt.title(
    "Comparison among Weighted, Equally Weighted & Market Cap weighted PF returns"
)
plt.grid(axis="y")
plt.legend()
plt.show()

# Notes:
# Nifty 50 is market cap weighted index
