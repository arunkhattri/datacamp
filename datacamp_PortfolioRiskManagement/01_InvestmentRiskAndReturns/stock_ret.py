from datetime import datetime
import pandas as pd
import numpy as np

#  import yfinance as yf
from pandas_datareader import data as web
import matplotlib.pyplot as plt
from scipy.stats import skew
from scipy.stats import kurtosis
from scipy import stats

start = datetime(2016, 4, 10)
end = datetime(2021, 4, 15)

ticker = "TATAMOTORS.NS"

#  tm_nse = yf.Ticker(ticker)
#
#  tm = tm_nse.history(period='max',)
#  print(tm.info())

df_tm = web.DataReader(ticker, "yahoo", start, end)
print(df_tm.info())

df_tm["returns"] = df_tm["Adj Close"].pct_change()
print(df_tm["returns"].head())

# Visualizing return distribution
plt.hist(df_tm.returns.dropna(), bins=75, density=False)
plt.show()

plt.hist(df_tm.returns.dropna(), bins=75)
plt.show()

df_tm.returns.plot()
plt.show()

# average daily returns
avg_daily_ret = np.mean(df_tm.returns)
print(f"Average Daily returns: {avg_daily_ret * 100:.2f} %")

# annual return, assuming 252 trading days
ann_ret = ((1 + np.mean(df_tm.returns)) ** 252) - 1
print(f"Annual Return: {ann_ret * 100:.2f} %")

# volatility
volatility = np.std(df_tm.returns)
print(f"Volatility: {volatility * 100:.2f}")

variance = np.std(df_tm.returns) ** 2
print(f"Variance: {variance * 10000:.2f}")

# annualized volatility of returns
ann_vol_ret = np.std(df_tm.returns) * np.sqrt(252)
print(f"Annualized volatility of returns: {ann_vol_ret * 100:.2f}")

# skewness of returns
skew_ret = skew(df_tm.returns.dropna())
print(f"Skewness of returns: {skew_ret:.3f}")

# Note: skewness is higher than 0, suggesting non-normality

# Kurtosis
# thickness of the tails of a distribution
# proxy for the outliers of the data
# Most financial returns are leptokurtic
# Leptokurtic: when a distribution has positive excess Kurtosis
# Excess Kurosis: Subtract 3 from the sample kurtosis to calculatie the excess
# kurtosis.
k_ret = kurtosis(df_tm.returns.dropna())  # calculates excess kurtosis
print(f"Third moment (Kurtosis) : {k_ret:.3f}")

# Testing for

# Note:  high kurtosis ~ high risk
p_value = stats.shapiro(df_tm.returns.dropna())[1]
if p_value <= 0.5:
    print("Null hypothesis of normality is rejected.")
else:
    print("Null hypothesis of normality is accepted.")
