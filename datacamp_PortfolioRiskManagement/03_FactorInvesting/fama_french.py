import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
import yfinance as yf


# define the parameters
RISKY_ASSETS = "FB"
START_DATE = "2013-12-31"
END_DATE = "2018-12-31"

# load the data from the source CSV file and keep only the monthly data
factor_df = pd.read_csv("./data/F-F_Research_Data_Factors.CSV", skiprows=3)
print(factor_df.info())

STR_TO_MATCH = " Annual Factors: January-December "
indices = factor_df.iloc[:, 0] == STR_TO_MATCH
start_of_annual = factor_df[indices].index[0]
factor_df = factor_df[factor_df.index < start_of_annual]

print(factor_df.info())

# rename the columns of the dataframe, set a datetime index and filter by dates
factor_df.columns = ["date", "mkt", "smb", "hml", "rf"]
factor_df["date"] = pd.to_datetime(factor_df["date"], format="%Y%m").dt.strftime(
    "%Y-%m"
)
factor_df = factor_df.set_index("date")
factor_df = factor_df.loc[START_DATE:END_DATE]
print(factor_df.head())

# convert the values into numeric and divide by 100
factor_df = factor_df.apply(pd.to_numeric, errors="coerce").div(100)
print(f"\n{factor_df.head()}")

# download the prices of the risky asset
asset_df = yf.download(RISKY_ASSETS, start=START_DATE, end=END_DATE, adjusted=True)
print(f"\n{asset_df.head()}")

# resample the data on 'months'
asset_df_mth = asset_df.resample("M").last()
print(f"\n{asset_df_mth.head()}")

asset_df_mth["returns"] = np.log(
    asset_df_mth["Adj Close"] / asset_df_mth["Adj Close"].shift(1)
)
asset_df_mth.dropna(inplace=True)
print(f"\n{asset_df_mth.head()}")

y = asset_df_mth["returns"]
y.index = y.index.strftime("%Y-%m")
y.name = "rtn"

# Merge the datasets and calculate the excess returns
ff_data = factor_df.join(y)
ff_data["excess_rtn"] = ff_data.rtn - ff_data.rf

# Estimate the 3-factor model
ff_model = smf.ols(formula="excess_rtn ~ mkt + smb + hml", data=ff_data).fit()
print(ff_model.summary())

# p-values and statistical significance
hml_p_val = ff_model.pvalues["hml"]
print(f"hml p value: {hml_p_val}")

# to test if it is significant, simply examine whether or not it is less than a given
# threshold, normally 0.05
print(hml_p_val < 0.05)

print(ff_model.params["hml"])

# alpha
asset_alpha = ff_model.params["Intercept"]
asset_alpha_annualized = ((1 + asset_alpha) ** 252) - 1
print(asset_alpha_annualized)
