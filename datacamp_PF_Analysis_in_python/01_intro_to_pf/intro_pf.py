import pandas as pd
import numpy as np
import yfinance as yf
import matplotlib.pyplot as plt
import seaborn as sns

akk_pf = pd.read_csv("./../data/210503_pf.csv")
akk_pf


total_investment = np.sum(akk_pf.val_at_cost)
total_investment

akk_pf['wt'] = akk_pf.val_at_cost / total_investment
akk_pf

# sanity check
akk_pf.wt.sum()


tickers = akk_pf.ticker.tolist()
col_names = tickers
tickers.append("%5ENSEI")  # Nifty
tickers

# get data for 2years
pf = yf.download(tickers, period='2y')['Adj Close']
pf.info()

# drop NA
pf.dropna(inplace=True)
pf.info()

# make colnames readable
col_names = [x[:-3] if x[-3:] == '.NS' else 'NIFTY' for x in tickers ]
pf.columns = col_names
pf.info()

# percent returns
pf_perc_rets = pf.pct_change()
pf_perc_rets

pf_cum_rets = (1 + pf_perc_rets).cumprod()
pf_cum_rets

fig, ax = plt.subplots()
for col in col_names:
    ax.plot(pf_cum_rets.index, pf_cum_rets[col], label=col)
ax.legend()
ax.grid('y')
plt.show()

# weighted return
weights = akk_pf.wt.tolist()
wt_rets = weights * pf_perc_rets.iloc[:,:-1]
pf_rets = wt_rets.sum(axis=1)
pf_rets_cum = (1 + pf_rets[1:]).cumprod()  # cumulative rets
nifty_rets = pf_perc_rets.iloc[:, -1]
nifty_rets_cum = (1 + nifty_rets[1:]).cumprod()  # cumulative rets

fig, ax = plt.subplots()
ax.plot(nifty_rets_cum.index, nifty_rets_cum, label='nifty')
ax.plot(pf_rets_cum.index, pf_rets_cum, label='pf')
ax.legend()
plt.show()


# Portfolio returns
mean_pf_rets = pf_perc_rets.iloc[:, :-1].mean()
mean_pf_rets
port_rets = np.sum(mean_pf_rets * weights)
print(f"Portfolio return: {port_rets * 100:.2f}")

# measuring risk of a portfolio
pf_rets = pf.iloc[:, :-1].pct_change()
pf_rets.head()

# covariance matrix for the daily returns data
cov_matrix_d = pf_rets.cov()
# annualized it
cov_mat_ann = cov_matrix_d * 250
cov_mat_ann

# plot covariance
mask = np.zeros_like(cov_mat_ann)
mask[np.triu_indices_from(mask)] = True
sns.heatmap(cov_mat_ann, mask=mask, square=True)
plt.show()

# calculate the variance
pf_variance = np.dot(np.array(weights).T, np.dot(cov_mat_ann, weights))
print(f"Portfolio varaiance: {pf_variance * 100:.2f}% ")

pf_std = np.sqrt(pf_variance)
print(f"Portfolio std: {pf_std * 100:.2f}%")
